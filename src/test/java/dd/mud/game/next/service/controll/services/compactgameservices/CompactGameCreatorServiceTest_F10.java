package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.controll.services.compactgameservices.controlobject.CharacterPosTuple;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.repositoryservices.*;
import dd.mud.game.next.service.entity.model.*;
import dd.mud.game.next.service.entity.model.Character;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class CompactGameCreatorServiceTest_F10 extends AbstractTestCompactGameCreator {

    @Mock
    GameRepositoryService gameRepositoryService;

    @Mock
    ItemRepositoryService itemRepositoryService;

    @Mock
    RoomRepositoryService roomRepositoryService;

    @Mock
    ItemRoomRepositoryService itemRoomRepositoryService;

    @Mock
    CharacterRepositoryService characterRepositoryService;

    @Mock
    CharacterRoomRepositoryService characterRoomRepositoryService;

    @Mock
    PlayerGameBanRepositoryService playerGameBanRepositoryService;

    @Mock
    GameActionRepositoryService gameActionRepositoryService;

    @Mock
    UserDataRepositoryService userDataRepositoryService;

    @Mock
    ReceiveCharacterJoinService receiveCharacterJoinService;

    @Mock
    ItemCharacterRepositoryService itemCharacterRepositoryService;

    @InjectMocks
    CompactGameCreatorService compactGameCreatorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createCompactGame() {
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        Long gameId = compactGame.getGameId();
        Long userId = 5L;

        when(itemCharacterRepositoryService.getItemCharactersByGameId(gameId)).thenReturn(new ArrayList<>());
        when(roomRepositoryService.getRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                Room.builder()
                        .roomName(compactGame.getRooms().get(0).getRoomName())
                        .coordx(compactGame.getRooms().get(0).getCoordx())
                        .coordy(compactGame.getRooms().get(0).getCoordy())
                        .descrip(compactGame.getRooms().get(0).getDescription())
                        .id(compactGame.getRooms().get(0).getRoomId())
                        .gameId(gameId)
                        .build())));
        when(itemRoomRepositoryService.getItemRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                ItemRoom.builder().gameId(gameId).itemId(1L).roomId(1L).build()
        )));
        when(characterRoomRepositoryService.getCharacterRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                CharakterRoom.builder()
                        .roomId(compactGame.getRooms().get(0).getRoomId())
                        .gameId(gameId)
                        .charakterId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                        .build()
        )));

        when(gameActionRepositoryService.getAllByGameIdAndIsGlobal(gameId, true)).thenReturn(new ArrayList<>(Arrays.asList(
                GameAction.builder()
                        .actionName(compactGame.getGlobalActions().get(0).getActionName())
                        .actionResult(compactGame.getGlobalActions().get(0).getActionResult())
                        .gameId(gameId)
                        .roomId(compactGame.getRooms().get(0).getRoomId())
                        .id(1L)
                        .build()
        )));

        when(receiveCharacterJoinService.getCurrentCharacterWithPos(gameId,userId)).thenReturn(CharacterPosTuple.builder().x(compactGame.getRooms().get(0).getCoordx()).y(compactGame.getRooms().get(0).getCoordy()).compactCharacter(CompactGame.CompactRoom.CompactCharacter.builder()
                .id(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                .playerId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())
                .personality(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPersonality())
                .hp(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getHp())
                .description(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDescription())
                .damage(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDamage())
                .name(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getName())
                .userName(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getUserName())
                .raceId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getRaceId())
                .classId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getClassId())
                .characterItems(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getCharacterItems())
                .build()).build());

        when(gameActionRepositoryService.getAllByGameIdAndIsGlobal(gameId, false)).thenReturn(new ArrayList<>(Arrays.asList(
                GameAction.builder()
                        .actionName(compactGame.getRooms().get(0).getActions().get(0).getActionName())
                        .actionResult(compactGame.getRooms().get(0).getActions().get(0).getActionResult())
                        .gameId(gameId)
                        .roomId(compactGame.getRooms().get(0).getRoomId())
                        .id(1L)
                        .build()
        )));

        when(playerGameBanRepositoryService.getBannedPlayerIds(anyLong())).thenReturn(compactGame.getBannedPlayers());
        when(gameRepositoryService.getUserIdByGameId(gameId)).thenReturn(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId());

        when(userDataRepositoryService.getUsernameByUserId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())).thenReturn(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getUserName());

        when(itemRepositoryService.getItemsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(Item.builder()
                .itemName("testItem")
                .itemType("testItemType")
                .itemValue(-1L)
                .build()
        )));

        when(characterRepositoryService.findCharactersByCharacterIds(new ArrayList<>(Arrays.asList(
                compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId()))))
                .thenReturn(new ArrayList<>(Arrays.asList(
                        Character.builder()
                                .id(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                                .chLevel(null)
                                .classId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getClassId())
                                .raceId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getRaceId())
                                .damage(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDamage())
                                .description(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDescription())
                                .gameId(gameId)
                                .hp(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getHp())
                                .userId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())
                                .chName(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getName())
                                .personality(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPersonality())
                                .build()
                )));

        when(itemRepositoryService.findItemsByItemIds(any())).thenReturn(new ArrayList<>(Arrays.asList(
                Item.builder()
                        .gameId(gameId)
                        .itemName(compactGame.getRooms().get(0).getCompactItems().get(0).getItemName())
                        .itemType(compactGame.getRooms().get(0).getCompactItems().get(0).getItemType())
                        .itemValue(compactGame.getRooms().get(0).getCompactItems().get(0).getItemValue())
                        .id(1L)
                        .build()
        )));

        when(gameActionRepositoryService.gameActions(gameId)).thenReturn(GameAction.builder().actionResult(compactGame.getGlobalActions().get(0).getActionResult()).actionName(compactGame.getGlobalActions().get(0).getActionName()).build());

        CompactGame actualCompactGame = compactGameCreatorService.createCompactGame(userId, gameId);
        assertEquals(actualCompactGame, compactGame);
    }
}