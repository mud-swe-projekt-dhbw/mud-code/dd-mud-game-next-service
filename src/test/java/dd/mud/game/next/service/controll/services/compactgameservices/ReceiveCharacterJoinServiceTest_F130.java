package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.compactgameservices.controlobject.CharacterPosTuple;
import dd.mud.game.next.service.controll.services.repositoryservices.*;
import dd.mud.game.next.service.entity.model.*;
import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ReceiveCharacterJoinServiceTest_F130 extends AbstractTestCompactGameCreator {

    @Mock
    CharacterRepositoryService characterRepositoryService;

    @Mock
    CharacterRoomRepositoryService characterRoomRepositoryService;

    @Mock
    ItemRepositoryService itemRepositoryService;

    @Mock
    RoomRepositoryService roomRepositoryService;

    @Mock
    ItemCharacterRepositoryService itemCharacterRepositoryService;

    @Mock
    UserDataRepositoryService userDataRepositoryService;

    @InjectMocks
    ReceiveCharacterJoinService receiveCharacterJoinService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCurrentCharacterWithPosTest() {
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        Long gameId = compactGame.getGameId();
        Long userId = compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId();
        when(characterRepositoryService.findCharacterByGameIdAndUserId(gameId, userId)).thenReturn(Character.builder()
                .id(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                .chLevel(null)
                .classId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getClassId())
                .raceId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getRaceId())
                .damage(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDamage())
                .description(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDescription())
                .gameId(gameId)
                .hp(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getHp())
                .userId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())
                .chName(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getName())
                .personality(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPersonality())
                .build());

        when(roomRepositoryService.getRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                Room.builder()
                        .roomName(compactGame.getRooms().get(0).getRoomName())
                        .coordx(compactGame.getRooms().get(0).getCoordx())
                        .coordy(compactGame.getRooms().get(0).getCoordy())
                        .descrip(compactGame.getRooms().get(0).getDescription())
                        .id(compactGame.getRooms().get(0).getRoomId())
                        .gameId(gameId)
                        .build())));

        when(itemRepositoryService.getItemsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(Item.builder().itemName("testItemName").itemType("testItem").itemValue(1L).id(1L).build(),
                Item.builder().itemName("testItem2").itemType("testItemType2").itemValue(-1L).id(2L).build()
        )));

        when(characterRoomRepositoryService.getCharacterRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                CharakterRoom.builder()
                        .gameId(gameId)
                        .charakterId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                        .roomId(compactGame.getRooms().get(0).getRoomId())
                        .build()
        )));

        when(itemCharacterRepositoryService.getItemCharactersByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                ItemCharacter.builder()
                        .gameId(gameId)
                        .chId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                        .itemId(1L)
                        .build()
        )));

        when(userDataRepositoryService.getUsernameByUserId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())).thenReturn(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getUserName());

        CompactGame.CompactRoom.CompactItem expectedCharacterItem = CompactGame.CompactRoom.CompactItem.builder()
                .itemValue(1L)
                .itemType("testItem")
                .itemName("testItemName")
                .build();
        CompactGame.CompactRoom.CompactCharacter compactCharacter =
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .playerId(1L)
                        .characterItems(new ArrayList<>(Arrays.asList(expectedCharacterItem)))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build();
        CharacterPosTuple characterPosTuple = CharacterPosTuple.builder()
                .compactCharacter(compactCharacter)
                .x(compactGame.getRooms().get(0).getCoordx())
                .y(compactGame.getRooms().get(0).getCoordy())
                .build();
        assertEquals(receiveCharacterJoinService.getCurrentCharacterWithPos(gameId, userId), characterPosTuple);
        verify(characterRoomRepositoryService, times(0)).saveNewCharacterRoom(any(), any(), any());
    }


    /*@Test
    void getCurrentCharacterWithPosNoCharacterInRoomTest() {
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        Long gameId = compactGame.getGameId();
        Long userId = compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId();
        when(characterRepositoryService.findCharacterByGameIdAndUserId(gameId, userId)).thenReturn(Character.builder()
                .id(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                .chLevel(null)
                .classId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getClassId())
                .raceId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getRaceId())
                .damage(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDamage())
                .description(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getDescription())
                .gameId(gameId)
                .hp(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getHp())
                .userId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())
                .chName(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getName())
                .personality(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPersonality())
                .build());

        when(roomRepositoryService.getRoomsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                Room.builder()
                        .roomName(compactGame.getRooms().get(0).getRoomName())
                        .coordx(compactGame.getRooms().get(0).getCoordx())
                        .coordy(compactGame.getRooms().get(0).getCoordy())
                        .descrip(compactGame.getRooms().get(0).getDescription())
                        .id(compactGame.getRooms().get(0).getRoomId())
                        .gameId(gameId)
                        .build())));

        when(itemRepositoryService.getItemsByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(Item.builder().itemName("testItemName").itemType("testItem").itemValue(1L).id(1L).build(),
                Item.builder().itemName("testItem2").itemType("testItemType2").itemValue(-1L).id(2L).build()
        )));

        //id should not equal an existing id
        when(characterRoomRepositoryService.getCharacterRoomsByGameId(gameId)).thenReturn(new ArrayList<>());

        Mockito.doNothing().when(characterRoomRepositoryService).saveNewCharacterRoom(gameId, compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId(), 1L);

        when(itemCharacterRepositoryService.getItemCharactersByGameId(gameId)).thenReturn(new ArrayList<>(Arrays.asList(
                ItemCharacter.builder()
                        .gameId(gameId)
                        .chId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId())
                        .itemId(1L)
                        .build()
        )));

        when(userDataRepositoryService.getUsernameByUserId(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId())).thenReturn(compactGame.getRooms().get(0).getCharactersInRoom().get(0).getUserName());

        CompactGame.CompactRoom.CompactItem expectedCharacterItem = CompactGame.CompactRoom.CompactItem.builder()
                .itemValue(1L)
                .itemType("testItem")
                .itemName("testItemName")
                .build();
        CompactGame.CompactRoom.CompactCharacter compactCharacter =
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .playerId(1L)
                        .characterItems(new ArrayList<>(Arrays.asList(expectedCharacterItem)))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build();
        CharacterPosTuple characterPosTuple = CharacterPosTuple.builder()
                .compactCharacter(compactCharacter)
                .x(compactGame.getRooms().get(0).getCoordx())
                .y(compactGame.getRooms().get(0).getCoordy())
                .build();

        assertEquals(receiveCharacterJoinService.getCurrentCharacterWithPos(gameId, userId), characterPosTuple);
        verify(characterRoomRepositoryService, times(1)).saveNewCharacterRoom(gameId, compactGame.getRooms().get(0).getCharactersInRoom().get(0).getId(), userId);
    }*/

    @Test
    void getCurrentCharacterWithPosWithNoCharacterTest(){
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        Long gameId = compactGame.getGameId();
        Long userId = compactGame.getRooms().get(0).getCharactersInRoom().get(0).getPlayerId();
        when(characterRepositoryService.findCharacterByGameIdAndUserId(gameId, userId)).thenReturn(null);

        assertNull(receiveCharacterJoinService.getCurrentCharacterWithPos(gameId,userId));
    }
}