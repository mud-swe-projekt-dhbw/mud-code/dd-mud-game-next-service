package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.ItemRoom;
import dd.mud.game.next.service.entity.repository.ItemRoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ItemRoomRepositoryServiceTest_F10 {

    @Mock
    ItemRoomRepository itemRoomRepository;

    @InjectMocks
    ItemRoomRepositoryService itemRoomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewItemRoom() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;

        ItemRoom itemRoom = ItemRoom.builder().itemId(itemId).roomId(roomId).gameId(gameId).build();

        when(itemRoomRepository.save(itemRoom)).thenReturn(itemRoom);

        when(itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId, itemId, gameId)).thenReturn(false);

        itemRoomRepositoryService.saveNewItemRoom(roomId, itemId, gameId);

        verify(itemRoomRepository, times(1)).save(itemRoom);
    }

    @Test
    void getItemRoomsByGameId() {
        Long gameId = 100L;
        Long itemId = 10L;
        Long roomId = 1L;
        List<ItemRoom> itemRooms = new ArrayList<ItemRoom>(Arrays.asList(
                ItemRoom.builder().roomId(roomId).itemId(itemId).gameId(gameId).build(),
                ItemRoom.builder().roomId(roomId).itemId(11L).gameId(gameId).build()));
        when(itemRoomRepository.getItemRoomsByGameId(gameId)).thenReturn(itemRooms);
        assertEquals(itemRooms, itemRoomRepositoryService.getItemRoomsByGameId(gameId));
    }
}