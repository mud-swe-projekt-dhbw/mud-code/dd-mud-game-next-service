package dd.mud.game.next.service.controll.services.gamelogic;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.repositoryservices.RoomRepositoryService;
import dd.mud.game.next.service.entity.model.Room;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class MovementServiceTest_F200 {
    @InjectMocks
    private MovementService movementService;

    @Mock
    private RoomRepositoryService roomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void checkIfDirection_true(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("w").build();
        boolean actual = movementService.checkIfDirection(mudKafkaMessage);
        assertTrue(actual);
    }

    @Test
    void checkIfDirection_false(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("keineRichtung").build();
        boolean actual = movementService.checkIfDirection(mudKafkaMessage);
        assertFalse(actual);
    }

    @Test
    void getNewCoordy_South(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("s").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = -1;
        Integer actual = movementService.getNewCoordy(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getNewCoordy_North(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("n").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = 1;
        Integer actual = movementService.getNewCoordy(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getNewCoordy_NoChangesCoordy(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("w").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = 0;
        Integer actual = movementService.getNewCoordy(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getNewCoordx_West(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("w").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = -1;
        Integer actual = movementService.getNewCoordx(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getNewCoordx_East(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("o").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = 1;
        Integer actual = movementService.getNewCoordx(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getNewCoordx_NoChangesCoordx(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(1L).message("n").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        Integer expected = 0;
        Integer actual = movementService.getNewCoordx(mudKafkaMessage, compactGame);
        assertEquals(expected,actual);
    }

    @Test
    void getUpdatedCompactGameAfterMoving (){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(2L).message("n").build();

        CompactGame firstCompactGame = getFirstCompactGame();
        CompactGame expectedCompactGame = getExpectedCompactGame();

        CompactGame updatedCompactgame = movementService.getUpdatedCompactGameAfterMoving(mudKafkaMessage, firstCompactGame);

        printCompactGame(firstCompactGame);
        printCompactGame(expectedCompactGame);
        printCompactGame(updatedCompactgame);
        assertEquals(expectedCompactGame, updatedCompactgame);
    }

    @Test
    void moveChaarcterGetAnswer(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(2L).message("n").build();

        CompactGame firstCompactGame = getFirstCompactGame();
        Room room = getRoomAfterMoving();
        when(roomRepositoryService.getRoomByGameIdAndCoordxAndCoordy(anyLong(), anyInt(), anyInt())).thenReturn(room);

        String answer = movementService.moveCharakterGetAnswer(mudKafkaMessage, firstCompactGame);
        String expected = "Du hast den Raum gewchselt. Raumname: testRoom2\n" +
                "roomDescription2\n" +
                "\n" +
                "Folgende Aktionen stehen dir zur Verfügung: \n" +
                "Bewegen mit: n, o, s, w\n" +
                "Aktuelle Informationen über in deinen Charakter: info, i";
        System.out.println(answer);
        assertEquals(expected, answer);
    }

    @Test
    void existsInDataBase(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(2L).message("n").build();
        CompactGame compactGame = getFirstCompactGame();
        when(roomRepositoryService.existsByGameIdAndCoordxAndCoordy(anyLong(),anyInt(),anyInt())).thenReturn(true);
        boolean actual = movementService.checkIfRoomExistsInDatabase(mudKafkaMessage, compactGame);
        assertTrue(actual);
    }

    private CompactGame getFirstCompactGame (){
        List<CompactGame.CompactRoom.CompactCharacter> emptyCompactCharacterList = new ArrayList<>();
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .characterItems(new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactItem.builder().itemValue(1L).itemType("testItem").itemName("testItemName").build())))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build()));
        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(0)
                        .compactItems(compactItems)
                        .charactersInRoom(compactCharacterList)
                        .roomId(1L)
                        .description("roomDescription")
                        .roomName("testRoom")
                        .actions(compactLocalActions)
                        .build(),
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(1)
                        .compactItems(compactItems)
                        .charactersInRoom(emptyCompactCharacterList)
                        .roomId(2L)
                        .description("roomDescription2")
                        .roomName("testRoom2")
                        .actions(compactLocalActions)
                        .build()));
        List<Long> bannedPlayers = new ArrayList<>(Arrays.asList(5L));
        List<CompactGame.CompactAction> compactActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testAction")
                        .actionResult("testActionResult")
                        .build()));
        CompactGame firstCompactGame = CompactGame.builder()
                .gameId(1L)
                .rooms(compactRoomList)
                .bannedPlayers(bannedPlayers)
                .dungeonMasterId(2L)
                .globalActions(compactActions).build();
        return firstCompactGame;
    }

    private CompactGame getExpectedCompactGame(){
        List<CompactGame.CompactRoom.CompactCharacter> emptyCompactCharacterList = new ArrayList<>();
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .characterItems(new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactItem.builder().itemValue(1L).itemType("testItem").itemName("testItemName").build())))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build()));
        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(0)
                        .compactItems(compactItems)
                        .charactersInRoom(emptyCompactCharacterList)
                        .roomId(1L)
                        .description("roomDescription")
                        .roomName("testRoom")
                        .actions(compactLocalActions)
                        .build(),
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(1)
                        .compactItems(compactItems)
                        .charactersInRoom(compactCharacterList)
                        .roomId(2L)
                        .description("roomDescription2")
                        .roomName("testRoom2")
                        .actions(compactLocalActions)
                        .build()));
        List<Long> bannedPlayers = new ArrayList<>(Arrays.asList(5L));
        List<CompactGame.CompactAction> compactActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testAction")
                        .actionResult("testActionResult")
                        .build()));
        CompactGame firstCompactGame = CompactGame.builder()
                .gameId(1L)
                .rooms(compactRoomList)
                .bannedPlayers(bannedPlayers)
                .dungeonMasterId(2L)
                .globalActions(compactActions).build();
        return firstCompactGame;
    }

    private Room getRoomAfterMoving(){
        List<CompactGame.CompactRoom.CompactCharacter> emptyCompactCharacterList = new ArrayList<>();
        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));
        Room room = Room.builder()
                .coordx(0)
                .coordy(1)
                .roomName("testRoom2")
                .descrip("roomDescription2")
                .gameId(1L)
                .id(2L)
                .build();
        return room;
    }

    private void printCompactGame (CompactGame compactGame){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(compactGame));
    }

}