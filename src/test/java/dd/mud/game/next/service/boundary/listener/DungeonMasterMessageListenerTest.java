package dd.mud.game.next.service.boundary.listener;

import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import dd.mud.game.next.service.controll.services.MudKafkaMessageCompactGameDistributorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class DungeonMasterMessageListenerTest extends AbstractTestCompactGameCreator {

    @Mock
    MudKafkaMessageCompactGameDistributorService mudKafkaMessageCompactGameDistributorService;

    @InjectMocks
    DungeonMasterMessageListener dungeonMasterMessageListener;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getUpdatedGameObjectFromDungeonMaster() {
        Mockito.doNothing().when(mudKafkaMessageCompactGameDistributorService).handleCompactGameInputFromDungeonMaster(AbstractTestCompactGameCreator.getTestCompactGame());
        dungeonMasterMessageListener.getUpdatedGameObjectFromDungeonMaster(AbstractTestCompactGameCreator.getTestCompactGame());
        verify(mudKafkaMessageCompactGameDistributorService, times(1)).handleCompactGameInputFromDungeonMaster(AbstractTestCompactGameCreator.getTestCompactGame());
    }
}