package dd.mud.game.next.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//needs to be removed before deployment, but it is necessary in general. This test makes sure that the Spring context loads
@SpringBootTest
class DdMudGameNextServiceApplicationTest {
    //test cannot communicate with the production server from your local machine. Make sure that the application.properties file is set correctly. Otherwise this test will fail
//    @Test void contextLoads() {
//    }
}