package dd.mud.game.next.service.helper;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.entity.model.ItemCharacter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractTestCompactGameCreator {

    public static CompactGame getTestCompactGame(){
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));

        CompactGame.CompactRoom.CompactItem charcterItem = CompactGame.CompactRoom.CompactItem.builder()
                .itemValue(1L)
                .itemType("testItem")
                .itemName("testItemName")
                .build();

        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .playerId(1L)
                        .characterItems(new ArrayList<>(Arrays.asList(charcterItem)))
                        .playerId(2L)
                        .characterItems(new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactItem.builder().itemValue(1L).itemType("testItem").itemName("testItemName").build())))
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build()));


        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(0)
                        .compactItems(compactItems)
                        .charactersInRoom(compactCharacterList)
                        .roomId(1L)
                        .description("roomDescription")
                        .roomName("testRoom")
                        .actions(compactLocalActions)
                        .build()));

        List<Long> bannedPlayers = new ArrayList<>(Arrays.asList(5L));

        List<CompactGame.CompactAction> compactActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testAction")
                        .actionResult("testActionResult")
                        .build()));


        CompactGame compactGame = CompactGame.builder()
                .gameId(1L)
                .rooms(compactRoomList)
                .bannedPlayers(bannedPlayers)
                .dungeonMasterId(2L)
                .globalActions(compactActions).build();
        return compactGame;
    }

    public static CompactGame.CompactRoom getTestCompactRoom(){
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .playerId(1L)
                        .characterItems(new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactItem.builder().itemValue(1L).itemType("testItem").itemName("testItemName").build())))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build()));
        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));

        CompactGame.CompactRoom compactRoom =CompactGame.CompactRoom.builder()
                .coordx(0)
                .coordy(0)
                .compactItems(compactItems)
                .charactersInRoom(compactCharacterList)
                .roomId(1L)
                .description("roomDescription")
                .roomName("testRoom")
                .actions(compactLocalActions)
                .build();
        return compactRoom;
    }

    public static MudKafkaMessage getTestMudKafkaMessage(){
        return MudKafkaMessage.builder().id(1L).gameId(1L).message("testMessage").build();
    }
}
