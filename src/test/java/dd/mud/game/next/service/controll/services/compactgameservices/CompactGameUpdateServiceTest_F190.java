package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.producer.ChatServiceUpdateProducer;
import dd.mud.game.next.service.boundary.producer.DungeonMasterUpdateProducer;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameUpdateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CompactGameUpdateServiceTest_F190 {

    @Mock
    DungeonMasterUpdateProducer dungeonMasterUpdateProducer;

    @Mock
    ChatServiceUpdateProducer chatServiceUpdateProducer;

    @InjectMocks
    CompactGameUpdateService compactGameUpdateService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void updateCompactGameOfWebserviceTest() {
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(3L).playerId(2L).build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList2 = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(4L).playerId(3L).build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.builder().coordx(21).coordy(31).charactersInRoom(compactCharacterList2).build()));
        CompactGame compactGame = CompactGame.builder().gameId(1L).rooms(compactRoomList).bannedPlayers(new ArrayList<>()).dungeonMasterId(2L).globalActions(new ArrayList<>()).build();

        Mockito.doNothing().when(dungeonMasterUpdateProducer).sendUpdatedGameToDungeonMaster(compactGame);
        Mockito.doNothing().when(chatServiceUpdateProducer).sendUpdatedGameToChatService(compactGame);

        compactGameUpdateService.updateCompactGameOfWebservice(compactGame);

        verify(dungeonMasterUpdateProducer, times(1)).sendUpdatedGameToDungeonMaster(compactGame);
        verify(chatServiceUpdateProducer, times(1)).sendUpdatedGameToChatService(compactGame);
    }
}