package dd.mud.game.next.service.boundary.listener;

import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import dd.mud.game.next.service.controll.services.MudKafkaMessageCompactGameDistributorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class GameMessageListenerTest extends AbstractTestCompactGameCreator {

    @Mock
    MudKafkaMessageCompactGameDistributorService mudKafkaMessageCompactGameDistributorService;

    @InjectMocks
    GameMessageListener gameMessageListener;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getNewGameMessageFromPlayer() {
        Mockito.doNothing().when(mudKafkaMessageCompactGameDistributorService).handleMudKafkaMessageInput(AbstractTestCompactGameCreator.getTestMudKafkaMessage());
        gameMessageListener.getNewGameMessageFromPlayer(AbstractTestCompactGameCreator.getTestMudKafkaMessage());
        verify(mudKafkaMessageCompactGameDistributorService, times(1)).handleMudKafkaMessageInput(AbstractTestCompactGameCreator.getTestMudKafkaMessage());
    }
}