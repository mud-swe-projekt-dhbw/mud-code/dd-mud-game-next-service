package dd.mud.game.next.service.boundary.producer;

import dd.mud.game.next.service.helper.AbstractGameNextServiceTest;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class PlayerAnswerProducerTest extends AbstractGameNextServiceTest {

    @Mock
    private KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    @InjectMocks
    private PlayerAnswerProducer playerAnswerProducer;

    @BeforeEach
    public void setUp () {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sendAnswerToPlayer () {
        MudKafkaMessage mudKafkaMessage = getMudKafkaMessage();

        doReturn(null).when(kafkaTemplate).send("gameAnswer.t", mudKafkaMessage);

        playerAnswerProducer.sendAnswerToPlayer(mudKafkaMessage);

        verify(kafkaTemplate, times(1)).send("gameAnswer.t", mudKafkaMessage);
        verifyNoMoreInteractions(kafkaTemplate);
    }

}