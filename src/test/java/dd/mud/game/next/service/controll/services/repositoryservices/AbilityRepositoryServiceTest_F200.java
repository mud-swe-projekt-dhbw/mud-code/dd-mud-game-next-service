package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Ability;
import dd.mud.game.next.service.entity.repository.AbilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class AbilityRepositoryServiceTest_F200 {

    @Mock
    AbilityRepository abilityRepository;

    @InjectMocks
    AbilityRepositoryService abilityRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void findAllByGameId() {
        List<Ability> ability = Collections.singletonList(Ability.builder().gameId(1L).abilityName("name").id(1L).build());
        when(abilityRepository.findAllByGameId(anyLong())).thenReturn(ability);
        List<Ability> actual = abilityRepositoryService.findAllByGameId(1L);
        assertEquals(ability, actual);

    }
}