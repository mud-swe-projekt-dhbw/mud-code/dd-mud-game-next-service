package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactCharakterService;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCreatorService;
import dd.mud.game.next.service.controll.services.repositoryservices.*;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlayerInformationServiceTest_F130 {

    @InjectMocks
    PlayerInformationService playerInformationService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void playerInformation() {
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(2L).gameId(1L).message("info").build();
        CompactGame compactGame = getFirstCompactGame();

        String answer = playerInformationService.getPlayerInformation(mudKafkaMessage, compactGame);
        String expected = "Name: testCharacter\n" +
                "HP: 1\n" +
                "Damage: 1\n" +
                "Description: test description\n" +
                "Personality: my test personality\n" +
                "Deine Items: testItem \n" +
                "Raumname: testRoom X: 0, Y: 0\n" +
                "Alle Charactere in deinem Raum: testCharacter ";
        System.out.println(answer);
        assertEquals(expected, answer);
    }

    @Test
    void playerInformation_BadCase() {
        //WrongPlayerId - Correct is 2L
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(1L).gameId(1L).message("info").build();
        CompactGame compactGame = getFirstCompactGame();

        String answer = playerInformationService.getPlayerInformation(mudKafkaMessage, compactGame);
        assertEquals(GameLogicStringConstants.ERROR_MESSAGE_INFO_SERVICE, answer);
    }

    private CompactGame getFirstCompactGame (){
        List<CompactGame.CompactRoom.CompactCharacter> emptyCompactCharacterList = new ArrayList<>();
        List<CompactGame.CompactAction> compactLocalActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testLocalAction")
                        .actionResult("testLocalActionResult")
                        .build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactCharacter.builder()
                        .id(3L)
                        .classId(1L)
                        .raceId(1L)
                        .userName("mister test")
                        .characterItems(new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactItem.builder().itemValue(1L).itemType("testItem").itemName("testItemName").build())))
                        .playerId(2L)
                        .name("testCharacter")
                        .damage(1L)
                        .description("test description")
                        .hp(1L)
                        .personality("my test personality")
                        .build()));
        List<CompactGame.CompactRoom.CompactItem> compactItems = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.CompactItem.builder()
                        .itemName("testItem")
                        .itemType("testItemType")
                        .itemValue(-1L)
                        .build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(0)
                        .compactItems(compactItems)
                        .charactersInRoom(compactCharacterList)
                        .roomId(1L)
                        .description("roomDescription")
                        .roomName("testRoom")
                        .actions(compactLocalActions)
                        .build(),
                CompactGame.CompactRoom.builder()
                        .coordx(0)
                        .coordy(1)
                        .compactItems(compactItems)
                        .charactersInRoom(emptyCompactCharacterList)
                        .roomId(2L)
                        .description("roomDescription2")
                        .roomName("testRoom2")
                        .actions(compactLocalActions)
                        .build()));
        List<Long> bannedPlayers = new ArrayList<>(Arrays.asList(5L));
        List<CompactGame.CompactAction> compactActions = new ArrayList<>(Arrays.asList(
                CompactGame.CompactAction.builder()
                        .actionName("testAction")
                        .actionResult("testActionResult")
                        .build()));
        CompactGame firstCompactGame = CompactGame.builder()
                .gameId(1L)
                .rooms(compactRoomList)
                .bannedPlayers(bannedPlayers)
                .dungeonMasterId(2L)
                .globalActions(compactActions).build();
        return firstCompactGame;
    }
}