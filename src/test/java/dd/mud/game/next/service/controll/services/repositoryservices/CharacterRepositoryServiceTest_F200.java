package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.repository.CharacterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class CharacterRepositoryServiceTest_F200 {

    @Mock
    CharacterRepository characterRepository;

    @InjectMocks
    CharacterRepositoryService characterRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void findCharacterByGameIdAndUserId() {
        Character character = Character.builder().chName("name").gameId(1L).hp(1L).damage(1L).chLevel(1L).classId(1L).id(1L).description("desc").raceId(1L).userId(1L).personality("perso").build();
        when(characterRepository.findCharacterByGameIdAndUserId(anyLong(), anyLong())).thenReturn(java.util.Optional.ofNullable(character));
        Character actual = characterRepositoryService.findCharacterByGameIdAndUserId(1L, 1L);
        assertEquals(character, actual);
    }

    @Test
    void findCharactersByCharacterIds() {
        Character character1 = Character.builder().chName("name").gameId(1L).hp(1L).damage(1L).chLevel(1L).classId(1L).id(1L).description("desc").raceId(1L).userId(1L).personality("perso").build();
        Character character2 = Character.builder().chName("name").gameId(1L).hp(1L).damage(1L).chLevel(1L).classId(1L).id(1L).description("desc").raceId(1L).userId(1L).personality("perso").build();
        List<Character> characterList = new ArrayList<>(Arrays.asList(character1, character2));
        List<Long> idList = new ArrayList<>(Arrays.asList(5L,4L));
        when(characterRepository.findCharactersByIdIn(any())).thenReturn(characterList);
        List<Character> actual = characterRepositoryService.findCharactersByCharacterIds(idList);
        assertEquals(characterList,actual);
    }
}