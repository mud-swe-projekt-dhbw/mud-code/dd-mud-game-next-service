package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.CharakterRoom;
import dd.mud.game.next.service.entity.repository.CharacterRepository;
import dd.mud.game.next.service.entity.repository.CharacterRoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class CharacterRoomRepositoryServiceTest_F190 {

    @Mock
    CharacterRoomRepository characterRoomRepository;

    @InjectMocks
    CharacterRoomRepositoryService characterRoomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCharacterRoomsByGameId() {
        List<CharakterRoom> charakterRoomList = new ArrayList<>(Arrays.asList(CharakterRoom.builder().roomId(1L).charakterId(1L).gameId(1L).build()));
        when(characterRoomRepository.getAllByGameId(anyLong())).thenReturn(charakterRoomList);
        List<CharakterRoom> actual = characterRoomRepositoryService.getCharacterRoomsByGameId(1L);
        assertEquals(charakterRoomList, actual);
    }
}