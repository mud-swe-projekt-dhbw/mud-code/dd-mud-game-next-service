package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCreatorService;
import dd.mud.game.next.service.controll.services.gamelogic.actions.ActionService;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MudKafkaMessageCompactGameUpdateServiceTest_F190 {

    @InjectMocks
    MudKafkaMessageCompactGameUpdateService mudKafkaMessageCompactGameUpdateService;

    @Mock
    MovementService movementService;

    @Mock
    PlayerInformationService playerInformationService;

    @Mock
    LeaveGameService leaveGameService;

    @Mock
    ActionService actionService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getUpdatedMessagesExitGame() {
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(1L)
                .message(GameLogicStringConstants.EXIT_GAME_COMMAND)
                .gameId(1L)
                .build();

        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        CompactGame updatedCompactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        updatedCompactGame.setGameId(999L);

        when(leaveGameService.leaveGame()).thenReturn(GameLogicStringConstants.EXIT_GAME_LAST_MESSAGE);
        when(leaveGameService.getUpdatedCompactGameAfterGameExit(mudKafkaMessage, compactGame)).thenReturn(updatedCompactGame);

        MudKafkaMessageCompactGamePair messageCompactGamePair =
            mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);

        mudKafkaMessage.setMessage(GameLogicStringConstants.EXIT_GAME_LAST_MESSAGE);

        verify(leaveGameService, times(1)).getUpdatedCompactGameAfterGameExit(mudKafkaMessage, compactGame);
        verify(leaveGameService, times(1)).leaveGame();
        assertEquals(GameLogicStringConstants.EXIT_GAME_LAST_MESSAGE, leaveGameService.leaveGame());
        assertEquals(mudKafkaMessage, messageCompactGamePair.getMudKafkaMessage());
        assertEquals(updatedCompactGame, messageCompactGamePair.getCompactGame());
    }

    @Test
    void getUpdatedMessagesMovementServiceDirection(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(1L)
                .message(GameLogicStringConstants.NORTH_BIG)
                .gameId(1L)
                .build();

        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        CompactGame updatedCompactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        updatedCompactGame.setGameId(999L);
        String outputMessage = "resultMessage";

        when(movementService.checkIfDirection(mudKafkaMessage)).thenReturn(true);
        when(movementService.checkIfRoomExistsInDatabase(mudKafkaMessage, compactGame)).thenReturn(true);
        when(movementService.moveCharakterGetAnswer(mudKafkaMessage, compactGame)).thenReturn(outputMessage);
        when(movementService.getUpdatedCompactGameAfterMoving(mudKafkaMessage, compactGame)).thenReturn(updatedCompactGame);

        MudKafkaMessageCompactGamePair messageCompactGamePair =
        mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);

        mudKafkaMessage.setMessage(outputMessage);

        verify(movementService, times(1)).moveCharakterGetAnswer(mudKafkaMessage, compactGame);
        verify(movementService, times(1)).getUpdatedCompactGameAfterMoving(mudKafkaMessage, compactGame);

        assertEquals(mudKafkaMessage, messageCompactGamePair.getMudKafkaMessage());
        assertEquals(updatedCompactGame, messageCompactGamePair.getCompactGame());
    }

    @Test
    void getUpdatedMessagesMovementServiceDirectionRoomNotFound(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(1L)
                .message(GameLogicStringConstants.NORTH_BIG)
                .gameId(1L)
                .build();

        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        when(movementService.checkIfDirection(mudKafkaMessage)).thenReturn(true);
        when(movementService.checkIfRoomExistsInDatabase(mudKafkaMessage, compactGame)).thenReturn(false);

        MudKafkaMessageCompactGamePair actualMessageCompactGamePair =
                mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);

        assertEquals(GameLogicStringConstants.ROOM_DOESNT_EXIST, actualMessageCompactGamePair.getMudKafkaMessage().getMessage());
        assertEquals(compactGame, actualMessageCompactGamePair.getCompactGame());
    }

    @Test
    void getUpdatedMessagesInfo(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(1L)
                .message(GameLogicStringConstants.INFO_BIG)
                .gameId(1L)
                .build();

        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        String outputMessage = "resultMessage";

        when(playerInformationService.getPlayerInformation(mudKafkaMessage, compactGame)).thenReturn(outputMessage);

        MudKafkaMessageCompactGamePair actualMessageCompactGamePair =
            mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);

        mudKafkaMessage.setMessage(outputMessage);

        verify(playerInformationService, times(1)).getPlayerInformation(mudKafkaMessage, compactGame);

        assertEquals(mudKafkaMessage, actualMessageCompactGamePair.getMudKafkaMessage());
        assertEquals(compactGame, actualMessageCompactGamePair.getCompactGame());
    }

    @Test
    void getUpdatedMessagesMovementServiceActionServiceCall(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder()
                .id(1L)
                .message(GameLogicStringConstants.INVALID_INPUT)
                .gameId(1L)
                .build();

        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        MudKafkaMessageCompactGamePair mudKafkaMessageCompactGamePair = MudKafkaMessageCompactGamePair.builder().compactGame(compactGame).mudKafkaMessage(mudKafkaMessage).build();
        when(actionService.processActionCommand(compactGame, mudKafkaMessage)).thenReturn(mudKafkaMessageCompactGamePair);

        MudKafkaMessageCompactGamePair actualMessageCompactGamePair =
                mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);

        verify(actionService, times(1)).processActionCommand(compactGame, mudKafkaMessage);
        assertEquals(GameLogicStringConstants.INVALID_INPUT, actualMessageCompactGamePair.getMudKafkaMessage().getMessage());
        assertEquals(compactGame, actualMessageCompactGamePair.getCompactGame());
    }

}