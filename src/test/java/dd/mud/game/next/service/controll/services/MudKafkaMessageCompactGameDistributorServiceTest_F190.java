package dd.mud.game.next.service.controll.services;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.boundary.producer.PlayerAnswerProducer;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCharacterJoinService;
import dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants;
import dd.mud.game.next.service.controll.services.gamelogic.MudKafkaMessageCompactGamePair;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCreatorService;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameUpdateService;
import dd.mud.game.next.service.controll.services.gamelogic.MudKafkaMessageCompactGameUpdateService;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class MudKafkaMessageCompactGameDistributorServiceTest_F190 {

    @Mock
    CompactGameUpdateService compactGameUpdateService;

    @Mock
    CompactGameCreatorService compactGameCreatorService;

    @Mock
    PlayerAnswerProducer playerAnswerProducer;

    @Mock
    MudKafkaMessageCompactGameUpdateService mudKafkaMessageCompactGameUpdateService;

    @Mock
    CompactGameCharacterJoinService compactGameCharacterJoinService;

    @InjectMocks
    MudKafkaMessageCompactGameDistributorService mudKafkaMessageCompactGameDistributorService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void handleMudKafkaMessageInputExistingGameTest() {
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(3L).playerId(2L).build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList2 = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(4L).playerId(3L).build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.builder().coordx(21).coordy(31).charactersInRoom(compactCharacterList2).build()));
        CompactGame compactGame = CompactGame.builder().gameId(1L).rooms(compactRoomList).bannedPlayers(new ArrayList<>()).dungeonMasterId(2L).globalActions(new ArrayList<>()).build();
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(1L).gameId(1L).message("do sth").build();

        Mockito.doNothing().when(compactGameUpdateService).updateCompactGameOfWebservice(compactGame);

        CompactGame updatedCompactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        updatedCompactGame.setBannedPlayers(new ArrayList<>(Arrays.asList(4L, 5L, 6L)));
        when(mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(any(), any())).thenReturn(MudKafkaMessageCompactGamePair.builder().mudKafkaMessage(mudKafkaMessage).compactGame(updatedCompactGame).build());
        when(compactGameCharacterJoinService.getCompactGameWithJoinedCharacter(mudKafkaMessage.getId(), compactGame)).thenReturn(updatedCompactGame);
        when(compactGameCharacterJoinService.getMudKafkaMessageAfterPlayerJoined(mudKafkaMessage, compactGame, GameLogicStringConstants.YOU_ARE_IN_ROOM)).thenReturn(mudKafkaMessage);

        mudKafkaMessageCompactGameDistributorService.games.put(1L, compactGame);
        mudKafkaMessageCompactGameDistributorService.handleMudKafkaMessageInput(mudKafkaMessage);

        verify(playerAnswerProducer, times(1)).sendAnswerToPlayer(mudKafkaMessage);
        verify(compactGameUpdateService, times(1)).updateCompactGameOfWebservice(updatedCompactGame);
        verify(mudKafkaMessageCompactGameUpdateService, times(1)).getUpdatedMessages(any(),any());
    }

    @Test
    void handleMudKafkaMessageInputExcludeBannedPlayersTest(){
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(3L).playerId(2L).build()));
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList2 = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(4L).playerId(3L).build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.builder().coordx(21).coordy(31).charactersInRoom(compactCharacterList2).build()));
        Long playerId = 2L;
        CompactGame compactGame = CompactGame.builder().gameId(1L).rooms(compactRoomList).bannedPlayers(new ArrayList<>(Arrays.asList(playerId))).dungeonMasterId(2L).globalActions(new ArrayList<>()).build();
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(playerId).gameId(1L).message("do sth").build();

        Mockito.doNothing().when(compactGameUpdateService).updateCompactGameOfWebservice(compactGame);
        when(mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(any(), any())).thenReturn(MudKafkaMessageCompactGamePair.builder().mudKafkaMessage(mudKafkaMessage).compactGame(compactGame).build());
        Mockito.doNothing().when(compactGameUpdateService).updateCompactGameOfWebservice(any());

        mudKafkaMessageCompactGameDistributorService.games.put(1L, compactGame);
        mudKafkaMessageCompactGameDistributorService.handleMudKafkaMessageInput(mudKafkaMessage);

        verify(compactGameCreatorService, times(0)).createCompactGame(1L, 1L);
        verify(compactGameUpdateService, times(0)).updateCompactGameOfWebservice(compactGame);
        verify(playerAnswerProducer, times(1)).sendAnswerToPlayer(any());
    }

    @Test
    void handleMudKafkaMessageInputNotExistingGameTest(){
        List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList2 = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.CompactCharacter.builder().id(4L).playerId(3L).build()));
        List<CompactGame.CompactRoom> compactRoomList = new ArrayList<>(Arrays.asList(CompactGame.CompactRoom.builder().coordx(0).coordy(0).charactersInRoom(compactCharacterList2).build()));
        Long playerId = 2L;
        CompactGame compactGame = CompactGame.builder().gameId(1L).rooms(compactRoomList).bannedPlayers(new ArrayList<>(Arrays.asList(playerId))).dungeonMasterId(2L).globalActions(new ArrayList<>()).build();

        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(1L).gameId(1L).message(GameLogicStringConstants.GAME_CREATE_COMMAND).build();
        when(compactGameCreatorService.createCompactGame(mudKafkaMessage.getId(), mudKafkaMessage.getGameId())).thenReturn(compactGame);
        Mockito.doNothing().when(compactGameUpdateService).updateCompactGameOfWebservice(compactGame);
        Mockito.doNothing().when(playerAnswerProducer).sendAnswerToPlayer(MudKafkaMessage.builder().gameId(1L).message(GameLogicStringConstants.GAME_WAS_CREATED).build());

        mudKafkaMessageCompactGameDistributorService.games.put(1000L, compactGame);
        mudKafkaMessageCompactGameDistributorService.handleMudKafkaMessageInput(mudKafkaMessage);

        verify(compactGameUpdateService, times(1)).updateCompactGameOfWebservice(compactGame);
        verify(compactGameCreatorService, times(1)).createCompactGame(1L ,1L);
        verify(playerAnswerProducer, times(1)).sendAnswerToPlayer(any());
    }

    @Test
    void handleMudKafkaMessageInputNotExistingGameFalseMessageTest(){
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().id(1L).gameId(1L).message("star;1").build();


        Mockito.doNothing().when(compactGameUpdateService).updateCompactGameOfWebservice(any());
        Mockito.doNothing().when(playerAnswerProducer).sendAnswerToPlayer(MudKafkaMessage.builder().gameId(1L).message(GameLogicStringConstants.GAME_CREATION_FAILED_OR_GAME_DELETED).build());

        mudKafkaMessageCompactGameDistributorService.handleMudKafkaMessageInput(mudKafkaMessage);

        verify(compactGameUpdateService, times(0)).updateCompactGameOfWebservice(any());
        verify(compactGameCreatorService, times(0)).createCompactGame(1L, 1L);
        verify(playerAnswerProducer, times(1)).sendAnswerToPlayer(any());
    }
}