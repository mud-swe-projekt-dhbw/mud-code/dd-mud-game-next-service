package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.helper.AbstractTestCompactGameCreator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class LeaveGameServiceTest_F140 {

    @InjectMocks
    LeaveGameService leaveGameService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getUpdatedCompactGameAfterGameExit() {
        MudKafkaMessage mudKafkaMessage = MudKafkaMessage.builder().gameId(1L).id(2L).message("message").build();
        CompactGame compactGame = AbstractTestCompactGameCreator.getTestCompactGame();

        CompactGame actualCompactGame = leaveGameService.getUpdatedCompactGameAfterGameExit(mudKafkaMessage, compactGame);

        CompactGame expectedCompactGame = AbstractTestCompactGameCreator.getTestCompactGame();
        expectedCompactGame.getRooms().get(0).getCharactersInRoom().remove(0);

        assertEquals(expectedCompactGame, actualCompactGame);

    }
}