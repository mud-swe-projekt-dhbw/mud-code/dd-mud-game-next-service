package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Room;
import dd.mud.game.next.service.entity.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class RoomRepositoryServiceTest_F10 {

    @Mock
    RoomRepository roomRepository;

    @InjectMocks
    RoomRepositoryService roomRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void getRoomsByGameId() {
        Long gameId = 100L;
        int coordx = 1;
        int coordy = -1;
        String descrip = "lorem bla bla bla";
        List<Room> rooms = new ArrayList<Room>(Arrays.asList(
                Room.builder().roomName("romm1").coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build(),
                Room.builder().roomName("room2").coordx(coordx).coordy(coordy).descrip(descrip).gameId(gameId).build()));
        when(roomRepository.getRoomsByGameId(gameId)).thenReturn(rooms);
        assertEquals(rooms, roomRepositoryService.getRoomsByGameId(gameId));
    }
}