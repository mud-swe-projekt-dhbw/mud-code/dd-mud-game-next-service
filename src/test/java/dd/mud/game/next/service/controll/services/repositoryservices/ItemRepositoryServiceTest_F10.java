package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Item;
import dd.mud.game.next.service.entity.repository.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class ItemRepositoryServiceTest_F10 {

    //todo: some repositoryService test cases are implemented in other services, can be copied for each repositoryService

    @Mock
    ItemRepository itemRepository;

    @InjectMocks
    ItemRepositoryService itemRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void getItemsByGameId() {
        Long gameId = 100L;
        List<Item> items = new ArrayList<Item>(Arrays.asList(
                Item.builder().itemType("testType").itemValue(1L).itemName("item1").gameId(gameId).build(),
                Item.builder().itemType("testType").itemValue(1L).itemName("item2").gameId(gameId).build()));
        when(itemRepository.getItemsByGameId(gameId)).thenReturn(items);
        assertEquals(items, itemRepositoryService.getItemsByGameId(gameId));
    }
}