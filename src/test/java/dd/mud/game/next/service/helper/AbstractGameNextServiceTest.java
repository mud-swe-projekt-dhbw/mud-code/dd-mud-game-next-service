package dd.mud.game.next.service.helper;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;

public abstract class AbstractGameNextServiceTest {

    protected MudKafkaMessage getMudKafkaMessage () {
        return MudKafkaMessage.builder()
                .id(1L)
                .message("Test Message")
                .gameId(2L)
                .build();
    }

    protected CompactGame getCompactGame () {
        return CompactGame.builder()
                .gameId(1L)
                .dungeonMasterId(2L)
                .build();
    }
}
