package dd.mud.game.next.service.boundary.producer;

import dd.mud.game.next.service.helper.AbstractGameNextServiceTest;
import dd.mud.game.next.service.boundary.model.CompactGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ChatServiceUpdateProducerTest extends AbstractGameNextServiceTest {

    @Mock
    private KafkaTemplate<String, CompactGame> kafkaTemplate;

    @InjectMocks
    public ChatServiceUpdateProducer chatServiceUpdateProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void sendUpdatedGameToChatService() {
        CompactGame compactGame = getCompactGame();

        doReturn(null).when(kafkaTemplate).send("chatCurrentGame.t", compactGame);

        chatServiceUpdateProducer.sendUpdatedGameToChatService(compactGame);

        verify(kafkaTemplate, times(1)).send("chatCurrentGame.t", compactGame);
        verifyNoMoreInteractions(kafkaTemplate);
    }
}