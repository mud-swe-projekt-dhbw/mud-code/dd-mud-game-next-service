package dd.mud.game.next.service.controll.services.gamelogic;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * this class processes player information requests
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayerInformationService {

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns an answer for the player when he wants to have some actual information about his character
     */
    public String getPlayerInformation(MudKafkaMessage mudKafkaMessage, CompactGame compactGame) {

        CompactGame.CompactRoom.CompactCharacter compactCharacter = null;
        CompactGame.CompactRoom compactRoom = null;

        for (CompactGame.CompactRoom room : compactGame.getRooms()) {
            for (CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()) {
                if (character.getPlayerId().equals(mudKafkaMessage.getId())) {
                    compactRoom = room;
                    compactCharacter = character;
                    break;
                }
            }
        }

        if (compactCharacter != null && compactRoom != null) {
            String playerInformation = "";
            StringBuilder characterName = new StringBuilder();
            StringBuilder characterItems = new StringBuilder();

            for (CompactGame.CompactRoom.CompactItem compactItem : compactRoom.getCompactItems()) {
                characterItems.append(compactItem.getItemName()).append(GameLogicStringConstants.SPACE);
            }

            for (CompactGame.CompactRoom.CompactCharacter character : compactRoom.getCharactersInRoom()) {
                characterName.append(character.getName()).append(GameLogicStringConstants.SPACE);
            }

            playerInformation = "Name: " + compactCharacter.getName()
                    + "\nHP: " + compactCharacter.getHp()
                    + "\nDamage: " + compactCharacter.getDamage()
                    + "\nDescription: " + compactCharacter.getDescription()
                    + "\nPersonality: " + compactCharacter.getPersonality()
                    + "\nDeine Items: " + characterItems.toString()
                    + "\nRaumname: " + compactRoom.getRoomName() + " X: " + compactRoom.getCoordx() + ", Y: " + compactRoom.getCoordy()
                    + "\nAlle Charactere in deinem Raum: " + characterName.toString();

            return playerInformation;
        } else {
            return GameLogicStringConstants.ERROR_MESSAGE_INFO_SERVICE;
        }
    }
}