package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class PlayerGameBanId implements Serializable {

    Long playerId;

    Long gameId;
}
