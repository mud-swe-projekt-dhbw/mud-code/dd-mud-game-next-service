package dd.mud.game.next.service.boundary.producer;

import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayerAnswerProducer {

    private static final String SEND_BACK_TO_CWS_SERVICE_TOPIC = "gameAnswer.t";

    private final KafkaTemplate<String, MudKafkaMessage> kafkaTemplate;

    /**
     * Method for sending a player the answer following on an input
     * @param mudKafkaMessage answer for player
     */
    public void sendAnswerToPlayer (MudKafkaMessage mudKafkaMessage) {
        this.kafkaTemplate.send(SEND_BACK_TO_CWS_SERVICE_TOPIC, mudKafkaMessage);
    }
}
