package dd.mud.game.next.service.boundary.listener;

import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.MudKafkaMessageCompactGameDistributorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameMessageListener {

    private final MudKafkaMessageCompactGameDistributorService mudKafkaMessageCompactGameDistributorService;

    /**
     * Method called when a new message is coming from a player
     * @param mudKafkaMessage received message
     */
    @KafkaListener(topics = "gameGameMessage.t",
            groupId = "gameNextListeners",
            containerFactory = "playerMessagesContainerFactory")
    public void getNewGameMessageFromPlayer (MudKafkaMessage mudKafkaMessage) {
        System.out.println("Incoming game message ID: " + mudKafkaMessage.getId());
        mudKafkaMessageCompactGameDistributorService.handleMudKafkaMessageInput(mudKafkaMessage);
    }
}
