package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class CharakterRoomId implements Serializable {

    private Long charakterId;

    private Long roomId;

}
