package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Game;
import dd.mud.game.next.service.entity.repository.GameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameRepositoryService {

    private final GameRepository gameRepository;

    public List<Game> getGames () {
        return gameRepository.findAll();
    }

//    public Game getGameByGameName(String gameName) {
//        return gameRepository.getGameByGameName(gameName);
//    }
//
//    public List<String> getAllGameNames(){
//        List<Game> games = gameRepository.findAll();
//        List<String> gameNames=new ArrayList<>();
//        for (Game game: games){
//            gameNames.add(game.getGameName());
//        }
//        return gameNames;
//    }

    public Long getUserIdByGameId(Long gameId){
        return gameRepository.getGameById(gameId).getUserId();
    }

}