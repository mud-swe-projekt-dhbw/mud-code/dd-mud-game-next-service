package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.ClassAbility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassAbilityRepository extends CrudRepository<ClassAbility, Long> {

    boolean existsByAbilityIdAndClassIdAndGameId(Long classId, Long abilityId, Long gameId);
    List<ClassAbility> findAllByGameId (Long gameId);
}