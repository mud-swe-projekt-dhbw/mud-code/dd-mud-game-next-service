package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class RaceAbilityId implements Serializable {

    Long raceId;

    Long abilityId;
}
