package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.PlayerGameBan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerGameBanRepository extends CrudRepository<PlayerGameBan, Long> {
    List<PlayerGameBan> findAllByGameId (Long gameId);
}
