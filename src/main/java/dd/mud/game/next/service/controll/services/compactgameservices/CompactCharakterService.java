package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.repositoryservices.*;
import dd.mud.game.next.service.entity.model.*;
import dd.mud.game.next.service.entity.model.Character;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CompactCharakterService {


    private final CharacterRepositoryService characterRepositoryService;
    private final UserDataRepositoryService userDataRepositoryService;
    private final ItemRepositoryService itemRepositoryService;
    private final ItemCharacterRepositoryService itemCharacterRepositoryService;



    public List<CompactGame.CompactRoom.CompactCharacter> mapCharactersToCompactCharacters(List<Character> characters){
        return characters.stream().map(character -> createCompactCharacter(character.getGameId(), character.getUserId())).collect(Collectors.toList());
    }

    private CompactGame.CompactRoom.CompactCharacter createCompactCharacter(Long gameId, Long userId) {
        Character character = characterRepositoryService.findCharacterByGameIdAndUserId(gameId, userId);
        String userName = userDataRepositoryService.getUsernameByUserId(userId);

        List<ItemCharacter> itemCharacters = itemCharacterRepositoryService.getItemCharactersByGameId(gameId);
        List<ItemCharacter> itemsOfCharacter = new ArrayList<>();
        for (ItemCharacter itemCharacter: itemCharacters){
            if (itemCharacter.getChId().equals(character.getId())){
                itemsOfCharacter.add(itemCharacter);
            }
        }

        List<Item> items = itemRepositoryService.findItemsByItemIds(itemsOfCharacter.stream().map(ItemCharacter::getItemId).collect(Collectors.toList()));

        List<CompactGame.CompactRoom.CompactItem> compactItems = mapItemsToCompactItems(items);

        return CompactGame.CompactRoom.CompactCharacter.builder()
                        .name(character.getChName())
                        .hp(character.getHp())
                        .damage(character.getDamage())
                        .id(character.getId())
                        .classId(character.getClassId())
                        .raceId(character.getRaceId())
                        .playerId(character.getUserId())
                        .description(character.getDescription())
                        .personality(character.getPersonality())
                        .userName(userName)
                        .characterItems(compactItems)
                        .build();
    }


    private List<CompactGame.CompactRoom.CompactItem> mapItemsToCompactItems(List<Item> items){
        return items.stream()
                .map(item -> CompactGame.CompactRoom.CompactItem.builder()
                        .itemName(item.getItemName())
                        .itemType(item.getItemType())
                        .itemValue(item.getItemValue())
                        .build())
                .collect(Collectors.toList());
    }
}
