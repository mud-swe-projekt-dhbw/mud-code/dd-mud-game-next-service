package dd.mud.game.next.service.controll.services.gamelogic;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * This class contains all String constants that are sended to the player
 * If concatination of Strings is necessary, remind using String builder
 * **/

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GameLogicStringConstants {

    public static final String INVALID_INPUT ="Ungültige Eingabe!";

    public static final String GAME_WAS_CREATED = "Ein neues Spiel wurde angelegt. ";
    public static final String GAME_CREATION_FAILED_OR_GAME_DELETED = "Das Spiel in dem du gerade bist ist entweder nicht geladen oder wurde beendet.";
    public static final String PLAYER_BANNED = "Du wurdest von dem Spiel wegen Fehlverhalten ausgeschlossen und darfst keine Aktionen mehr durchführen";

    public static final String GAME_CREATE_COMMAND = "START!;";
    public static final String GAME_JOIN_COMMAND = "START!;";

    public static final String EXIT_GAME_LAST_MESSAGE = "Verlasse Spiel... Deine Spielstände werden gespeichert. Wir beehren Sie uns bald wieder!";
    public static final String EXIT_GAME_COMMAND = "EXIT!;";
    public static final String RESET_GAME_COMMAND = "RESET!;";
    public static final String RESET_MESSAGE = "RESET SUCCESSFULL";

    //Information Service
    public static final String INFO_BIG = "Info";
    public static final String INFO_SMALL = "info";
    public static final String INFO_SHORT = "i";
    public static final String SPACE = " ";
    public static final String ERROR_MESSAGE_INFO_SERVICE = "Es scheint ein Fehler passiert zu sein!";


    //MovementService
    public static final String NORTH_BIG = "Norden";
    public static final String NORTH_SMALL = "norden";
    public static final String NORTH_SHORT = "n";
    public static final String EAST_BIG = "Osten";
    public static final String EAST_SMALL = "osten";
    public static final String EAST_SHORT = "o";
    public static final String SOUTH_BIG = "Süden";
    public static final String SOUTH_SMALL = "süden";
    public static final String SOUTH_SHORT = "s";
    public static final String WEST_BIG = "Westen";
    public static final String WEST_SMALL = "westen";
    public static final String WEST_SHORT = "w";
    public static final String ROOM_CHANGE_INFO = "Du hast den Raum gewchselt. Raumname: ";
    public static final String ROOM_DOESNT_EXIST = "In dieser Richtung existiert kein Raum mehr. Bewege dich in eine andere Richtung!";
    public static final String AVAILABLE_COMMANDS = "Folgende Aktionen stehen dir zur Verfügung: ";
    public static final String MOVE_WITH = "Bewegen mit: ";
    public static final String PLAYER_INFORMATION = "Aktuelle Informationen über in deinen Charakter: ";
    public static final String ZERO_ROOM_NOT_FOUND = "Beim Laden der ersten Raumbeschreibung ist ein Fehler aufgetreten!";
    public static final String YOU_ARE_IN_ROOM = "Willkommen im Spiel.\nDu bist im Raum: ";

    //ActionService
    public static final String ACTION_COMMAND_START = "Aktion: ";
    public static final String ACTION_COMMAND_NOT_FOUND_MESSAGE_TO_PLAYER = "Die von dir eingegebene Aktion ist leider nicht verfügbar. Hast du den Namen der Aktion auch richtig geschrieben?";

    public static final String NO_LOCAL_ACTION_AVAILABLE = "Es gibt leider keine Aktion, die von diesem Raum abhängt.";
    public static final String NO_GLOBAL_ACTION_AVAILABLE = "Es gibt leider keine globale Aktion mit diesem Namen im Spiel.";
    public static final String ACTION_NOT_ALLOWED = "Leider darfst du diese Aktion nicht ausführen";
    public static final String ACTION_NOT_AVAILABLE = "Diese Aktion ist hier nicht verfügbar";
    public static final String YOU_DO_NOT_HAVE_THE_CLASS_TO_DO_THIS_ACTION = "Dein Charakter hat leider nicht die richtige Klasse, um diese Aktion auszuführen";
    public static final String YOU_DO_NOT_HAVE_THE_RASSE_TO_DO_THIS_ACTION = "Dein Charakter hat leider nicht die richtige Rasse, um diese Aktion auszuführen";
    public static final String PROCESS_ACTION = "Resultat der Aktion: ";

}