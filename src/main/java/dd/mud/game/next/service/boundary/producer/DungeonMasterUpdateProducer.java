package dd.mud.game.next.service.boundary.producer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dd.mud.game.next.service.boundary.model.CompactGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DungeonMasterUpdateProducer {

    private static final String DUNGEON_MASTER_GAME_UPDATE_TOPIC = "dungeonMasterCurrentGame.t";

    private final KafkaTemplate<String, CompactGame> kafkaTemplate;

    /**
     * Method for sending an updated game object to the Dungeon Master service.
     * Gets called when a player makes an action that changes the game object.
     * @param compactGame updated game object
     */
    public void sendUpdatedGameToDungeonMaster (CompactGame compactGame) {
        printCompactGame(compactGame);
        this.kafkaTemplate.send(DUNGEON_MASTER_GAME_UPDATE_TOPIC, compactGame);
    }

    private void printCompactGame (CompactGame compactGame){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(compactGame));
    }
}
