package dd.mud.game.next.service.boundary.listener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.MudKafkaMessageCompactGameDistributorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DungeonMasterMessageListener {

    private final MudKafkaMessageCompactGameDistributorService mudKafkaMessageCompactGameDistributorService;

    /**
     * Method for receiving updated compact games from the Dungeon Master service
     * @param compactGame updated game object
     */
    @KafkaListener(topics = "gameDMCurrentGame.t",
            groupId = "gameNextListeners",
            containerFactory = "gameUpdateContainerFactory")
    public void getUpdatedGameObjectFromDungeonMaster (CompactGame compactGame) {
        printCompactGame(compactGame);
        mudKafkaMessageCompactGameDistributorService.handleCompactGameInputFromDungeonMaster(compactGame);
    }

    private void printCompactGame (CompactGame compactGame){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(compactGame));
    }
}
