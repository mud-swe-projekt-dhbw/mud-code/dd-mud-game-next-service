package dd.mud.game.next.service.controll.services.gamelogic.actions;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.gamelogic.MudKafkaMessageCompactGamePair;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants.ACTION_COMMAND_NOT_FOUND_MESSAGE_TO_PLAYER;
import static dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants.ACTION_NOT_ALLOWED;
import static dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants.ACTION_NOT_AVAILABLE;
import static dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants.INVALID_INPUT;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ActionService {

    private final ActionClassRaceCheckService actionClassRaceCheckService;
    private final ExecuteActionService executeActionService;

    public MudKafkaMessageCompactGamePair processActionCommand (CompactGame compactGame, MudKafkaMessage mudKafkaMessage) {
        String action = mudKafkaMessage.getMessage();
        CompactGame.CompactRoom.CompactCharacter compactCharacter = null;
        CompactGame.CompactRoom compactRoom = null;

        for (CompactGame.CompactRoom room : compactGame.getRooms()) {
            for (CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()) {
                if (character.getPlayerId().equals(mudKafkaMessage.getId())) {
                    compactCharacter = character;
                    compactRoom = room;
                }
            }
        }

        if (compactCharacter == null) {
            return createMudKafkaMessagecompactGamePair(compactGame, mudKafkaMessage, ACTION_COMMAND_NOT_FOUND_MESSAGE_TO_PLAYER);
        } else {

            switch (this.actionClassRaceCheckService.playerIsAllowedToMakeAction(action, mudKafkaMessage.getGameId(), compactCharacter)) {
                case ALLOWED:
                case NOT_AVAILABLE:
                    return executeAction(compactCharacter, compactRoom, compactGame, mudKafkaMessage, action);
                case FORBIDDEN:
                    return createMudKafkaMessagecompactGamePair(compactGame, mudKafkaMessage, ACTION_NOT_ALLOWED);
                default:
                    return createMudKafkaMessagecompactGamePair(compactGame, mudKafkaMessage, INVALID_INPUT);

            }
        }
    }

    private MudKafkaMessageCompactGamePair executeAction (CompactGame.CompactRoom.CompactCharacter compactCharacter,
                                                          CompactGame.CompactRoom compactRoom,
                                                          CompactGame compactGame,
                                                          MudKafkaMessage mudKafkaMessage,
                                                          String action) {
        Optional<CompactGame.CompactAction> compactAction = isActionAvailableHere(compactGame, action, compactRoom);

        if (compactAction.isPresent()) {
            return createMudKafkaMessagecompactGamePair(compactGame, mudKafkaMessage, this.executeActionService.executeActionAndReturnResultString(
               compactAction.get(), mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), compactCharacter
            ));
        } else {
            return createMudKafkaMessagecompactGamePair(compactGame, mudKafkaMessage, ACTION_NOT_AVAILABLE);
        }
    }

    private static MudKafkaMessageCompactGamePair createMudKafkaMessagecompactGamePair (CompactGame compactGame, MudKafkaMessage mudKafkaMessage, String message) {
        return MudKafkaMessageCompactGamePair.builder().compactGame(compactGame).mudKafkaMessage(
                MudKafkaMessage.builder()
                        .id(mudKafkaMessage.getId())
                        .gameId(mudKafkaMessage.getGameId())
                        .message(message)
                        .build())
                .build();
    }

    private Optional<CompactGame.CompactAction> isActionAvailableHere (CompactGame compactGame, String action, CompactGame.CompactRoom room) {
        for (CompactGame.CompactAction roomAction : room.getActions()) {
            if (roomAction.getActionName().equalsIgnoreCase(action)) return Optional.of(roomAction);
        }

        for (CompactGame.CompactAction globalAction : compactGame.getGlobalActions()) {
            if (globalAction.getActionName().equalsIgnoreCase(action)) return Optional.of(globalAction);
        }

        return Optional.empty();
    }
}