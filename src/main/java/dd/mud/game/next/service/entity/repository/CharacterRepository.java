package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Long> {
    boolean existsByUserIdAndGameId(Long userId, Long gameId);

    Optional<Character> findCharacterByGameIdAndUserId(Long gameId, Long userId);

    List<Character> findCharactersByIdIn(List<Long> ids);

    Optional<Character> findCharacterByUserIdAndGameId(Long userId, Long gameId);
}
