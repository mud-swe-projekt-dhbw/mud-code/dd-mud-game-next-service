package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(ItemCharacterId.class)
@Table(name = "itemCharakter", schema = "public")
public class ItemCharacter {

    @Id
    private Long chId;

    @Id
    private Long itemId;

    private Long gameId;
}