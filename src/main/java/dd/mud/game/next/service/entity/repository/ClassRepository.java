package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Class;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClassRepository extends CrudRepository<Class, Long> {

    boolean existsByClassNameAndGameId (String classname, Long gameId);

    List<Class> findAllByGameId(Long gameId);

    Optional<Class> findByClassNameAndGameId(String className, Long gameId);

    Class getById(Long id);
}
