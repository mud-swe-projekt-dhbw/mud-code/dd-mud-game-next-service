package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Ability;
import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.model.Class;
import dd.mud.game.next.service.entity.model.GameAction;
import dd.mud.game.next.service.entity.repository.AbilityRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AbilityRepositoryService {

    private final AbilityRepository abilityRepository;

     public List<Ability> findAllByGameId (Long gameId) {
         return abilityRepository.findAllByGameId(gameId);
     }
}
