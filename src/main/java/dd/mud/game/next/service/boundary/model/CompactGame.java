package dd.mud.game.next.service.boundary.model;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompactGame {

    private Long gameId;
    private List<CompactRoom> rooms;
    // Banned players are identified over the playerId
    private List<Long> bannedPlayers;
    private Long dungeonMasterId;
    List<CompactAction> globalActions;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CompactAction{
        private String actionName;
        private String actionResult;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CompactRoom {
        //@Getter
        private Long roomId;
        private String roomName;
        // Represented by the character ids in a room
        private List<CompactCharacter> charactersInRoom;
        private int coordx;
        private int coordy;
        private String description;
        private List<CompactAction> actions;
        private List<CompactItem> compactItems;

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        @Builder
        public static class CompactItem {
            private String itemType;
            private String itemName;
            private Long itemValue;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        @Builder
        public static class CompactCharacter {
            private Long id;
            private String name;
            private Long hp;
            private Long damage;
            private Long classId;
            private Long raceId;
            private Long playerId;
            private String userName;
            private String description;
            private String personality;
            private List<CompactItem> characterItems;
        }
    }
}
