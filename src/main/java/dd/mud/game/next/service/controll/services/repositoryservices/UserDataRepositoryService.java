package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.repository.UserDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UserDataRepositoryService {

    private final UserDataRepository userDataRepository;

    public String getUsernameByUserId(Long userId){
        return userDataRepository.getById(userId).getUsername();
    }
}