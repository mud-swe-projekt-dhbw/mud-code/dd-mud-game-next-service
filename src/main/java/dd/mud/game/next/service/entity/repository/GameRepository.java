package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {

    boolean existsByGameName(String gameName);

    Game findByGameName(String gameName);
    Game getGameByGameName(String gameName);

    List<Game> findAll();

    Game getGameById(Long id);
}