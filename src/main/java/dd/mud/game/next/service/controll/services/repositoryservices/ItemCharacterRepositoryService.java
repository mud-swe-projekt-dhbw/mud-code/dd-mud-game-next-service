package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.ItemCharacter;
import dd.mud.game.next.service.entity.repository.ItemCharacterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemCharacterRepositoryService {

    private final ItemCharacterRepository itemCharacterRepository;


    public List<ItemCharacter> getItemCharactersByGameId(Long gameId){
        return itemCharacterRepository.getAllByGameId(gameId);
    }
}
