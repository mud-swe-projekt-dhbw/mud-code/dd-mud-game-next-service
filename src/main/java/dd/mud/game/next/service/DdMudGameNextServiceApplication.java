package dd.mud.game.next.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudGameNextServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdMudGameNextServiceApplication.class, args);
	}

}
