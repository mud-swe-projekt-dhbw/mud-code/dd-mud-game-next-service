package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Race;
import dd.mud.game.next.service.entity.repository.RaceRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RaceRepositoryService {

    private final RaceRepository raceRepository;

    public List<Race> getRacesByGameId(Long gameId) {
        List<Race> races = new ArrayList<>();
        raceRepository.findAllByGameId(gameId).forEach(races::add);
        return races;
    }

    public Race getRaceByRaceId(Long id){
        return raceRepository.getById(id);
    }
}

