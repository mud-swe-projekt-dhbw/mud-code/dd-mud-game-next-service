package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.repository.ClassAbilityRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClassAbilityRepositoryService {

    private final ClassAbilityRepository classAbilityRepository;

}