package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.ItemRoom;
import dd.mud.game.next.service.entity.repository.ItemRoomRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemRoomRepositoryService {

    private final ItemRoomRepository itemRoomRepository;

    public void saveNewItemRoom (@NonNull Long roomId, @NonNull Long itemId, @NonNull Long gameId){
        if (!itemRoomRepository.existsByRoomIdAndItemIdAndGameId(roomId, itemId, gameId)){
            itemRoomRepository.save(
                    ItemRoom.builder().roomId(roomId).itemId(itemId).gameId(gameId).build());
        }
        else {
            //throw new ItemAlreadyExistsInRoomException();
        }
    }

    public List<ItemRoom> getItemRoomsByGameId(long gameId){
        return itemRoomRepository.getItemRoomsByGameId(gameId);
    }
}
