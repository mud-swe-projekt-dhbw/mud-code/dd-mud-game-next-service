package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ItemRoomId implements Serializable {

    private Long roomId;

    private Long itemId;
}
