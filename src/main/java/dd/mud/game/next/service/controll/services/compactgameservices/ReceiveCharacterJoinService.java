package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.compactgameservices.controlobject.CharacterPosTuple;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRoomRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.ItemCharacterRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.ItemRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.RoomRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.UserDataRepositoryService;
import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.model.CharakterRoom;
import dd.mud.game.next.service.entity.model.Item;
import dd.mud.game.next.service.entity.model.ItemCharacter;
import dd.mud.game.next.service.entity.model.Room;
import dd.mud.game.next.service.entity.model.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ReceiveCharacterJoinService {

    private static final int DEFAULT_X_COORDINATE = 0;
    private static final int DEFAULT_Y_COORDINATE = 0;

    private final CharacterRepositoryService characterRepositoryService;
    private final ItemRepositoryService itemRepositoryService;
    private final ItemCharacterRepositoryService itemCharacterRepositoryService;
    private final UserDataRepositoryService userDataRepositoryService;

    public CharacterPosTuple getCurrentCharacterWithPos (Long gameId, Long playerId) {
        Character character = this.characterRepositoryService.findCharacterByGameIdAndUserId(gameId, playerId);
        if (character == null) {
            return null;
        }
        List<Item> items = this.itemRepositoryService.getItemsByGameId(gameId);
        List<ItemCharacter> itemCharacters = this.itemCharacterRepositoryService.getItemCharactersByGameId(gameId);
        String userName = this.userDataRepositoryService.getUsernameByUserId(playerId);

        List<Long> itemIds = itemCharacters.stream().filter(itemCharacter -> itemCharacter.getChId().equals(character.getId()))
                .map(ItemCharacter::getItemId).collect(Collectors.toList());

        List<CompactGame.CompactRoom.CompactItem> compactItemsOfCharacter = new ArrayList<>();
        for (Item item : items) {
            for (Long itemId : itemIds) {
                if (item.getId().equals(itemId)) {
                    compactItemsOfCharacter.add(this.mapToCompactItem(item));
                    break;
                }
            }
        }


        CompactGame.CompactRoom.CompactCharacter compactCharacter = assembleCompactCharacter(character, userName, compactItemsOfCharacter);

        return CharacterPosTuple.builder()
                .compactCharacter(compactCharacter)
                .x(DEFAULT_X_COORDINATE)
                .y(DEFAULT_Y_COORDINATE)
                .build();
    }

    private CompactGame.CompactRoom.CompactCharacter assembleCompactCharacter(Character character, String userName, List<CompactGame.CompactRoom.CompactItem> compactItemsOfCharacter) {
        return CompactGame.CompactRoom.CompactCharacter.builder()
                    .name(character.getChName())
                    .hp(character.getHp())
                    .damage(character.getDamage())
                    .id(character.getId())
                    .classId(character.getClassId())
                    .raceId(character.getRaceId())
                    .playerId(character.getUserId())
                    .description(character.getDescription())
                    .personality(character.getPersonality())
                    .userName(userName)
                    .characterItems(compactItemsOfCharacter)
                    .build();
    }

    private CompactGame.CompactRoom.CompactItem mapToCompactItem(Item item) {
        return CompactGame.CompactRoom.CompactItem.builder()
                .itemName(item.getItemName())
                .itemType(item.getItemType())
                .itemValue(item.getItemValue())
                .build();
    }
}
