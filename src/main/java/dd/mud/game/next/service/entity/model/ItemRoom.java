package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

// noArgsConstructor is necessary for the Database
@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(ItemRoomId.class)
@Table(name = "itemRoom", schema = "public")
public class ItemRoom {

    @Id
    private Long roomId;

    @Id
    private Long itemId;

    private Long gameId;
}
