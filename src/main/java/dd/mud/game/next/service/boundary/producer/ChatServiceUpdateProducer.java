package dd.mud.game.next.service.boundary.producer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.gamelogic.PlayerInformationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChatServiceUpdateProducer {

    private static final String CHAT_SERVICE_GAME_UPDATE_TOPIC = "chatCurrentGame.t";

    private final KafkaTemplate<String, CompactGame> kafkaTemplate;

    /**
     * Method for sending the chat service an updated game object.
     * Gets called when an player makes a change to the game object through playing.
     * @param compactGame updated game object
     */
    public void sendUpdatedGameToChatService (CompactGame compactGame) {
        printCompactGame(compactGame);
        this.kafkaTemplate.send(CHAT_SERVICE_GAME_UPDATE_TOPIC, compactGame);
    }

    private void printCompactGame (CompactGame compactGame){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(compactGame));
    }
}
