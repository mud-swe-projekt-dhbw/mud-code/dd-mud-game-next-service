package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.PlayerGameBan;
import dd.mud.game.next.service.entity.repository.PlayerGameBanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayerGameBanRepositoryService {

    private final PlayerGameBanRepository playerGameBanRepository;

    public List<Long> getBannedPlayerIds(Long gameId){
        return this.playerGameBanRepository.findAllByGameId(gameId).stream().map(PlayerGameBan::getPlayerId).collect(Collectors.toList());
    }
}
