package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Room;
import dd.mud.game.next.service.entity.repository.RoomRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RoomRepositoryService {

    private final RoomRepository roomRepository;

    public List<Room> getRoomsByGameId(long gameId){
        return roomRepository.getRoomsByGameId(gameId);
    }

    public Room getRoomByGameIdAndCoordxAndCoordy(Long gameId, Integer coordx, Integer coordy){
        return roomRepository.getByGameIdAndCoordxAndCoordy(gameId, coordx, coordy);
    }

    public boolean existsByGameIdAndCoordxAndCoordy(Long gameId, Integer coordx, Integer coordy){
        return roomRepository.existsByGameIdAndCoordxAndCoordy(gameId, coordx, coordy);
    }
}