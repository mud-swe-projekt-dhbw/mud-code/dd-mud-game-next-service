package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.ItemCharacter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemCharacterRepository extends CrudRepository<ItemCharacter,Long> {

    List<ItemCharacter> getAllByGameId(Long gameId);
}
