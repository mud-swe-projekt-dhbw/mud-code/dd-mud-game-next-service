package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

// noArgsConstructor is necessary for the Database
@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "item", schema = "public")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String itemType;

    private String itemName;

    private Long itemValue;

    private Long gameId;
}

