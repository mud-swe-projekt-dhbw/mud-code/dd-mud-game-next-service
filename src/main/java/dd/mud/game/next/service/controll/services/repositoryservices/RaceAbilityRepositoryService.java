package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Ability;
import dd.mud.game.next.service.entity.model.RaceAbility;
import dd.mud.game.next.service.entity.repository.RaceAbilityRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RaceAbilityRepositoryService {

    private final RaceAbilityRepository raceAbilityRepository;

//    public RaceAbility findAllByGameId (Long gameId) {
//        return raceAbilityRepository.findAllByGameId(gameId);
//    }

}