package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.compactgameservices.controlobject.CharacterPosTuple;
import dd.mud.game.next.service.controll.services.repositoryservices.*;
import dd.mud.game.next.service.entity.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CompactGameCreatorService {

    private final GameRepositoryService gameRepositoryService;
    private final ItemRepositoryService itemRepositoryService;
    private final RoomRepositoryService roomRepositoryService;
    private final ItemRoomRepositoryService itemRoomRepositoryService;
    private final PlayerGameBanRepositoryService playerGameBanRepositoryService;
    private final GameActionRepositoryService gameActionRepositoryService;
    private final ReceiveCharacterJoinService receiveCharacterJoinService;

    public CompactGame createCompactGame(Long playerId, Long gameId){

        List<Room> rooms = roomRepositoryService.getRoomsByGameId(gameId);
        List<ItemRoom> itemRooms = itemRoomRepositoryService.getItemRoomsByGameId(gameId);
        List<GameAction> localActions = gameActionRepositoryService.getAllByGameIdAndIsGlobal(gameId, false);
        List<GameAction> globalGameActions = gameActionRepositoryService.getAllByGameIdAndIsGlobal(gameId, true);
        List<CompactGame.CompactAction> compactGlobalActions = getCompactGlobalActions(globalGameActions);
        List<Long> bannedPlayerIds = playerGameBanRepositoryService.getBannedPlayerIds(gameId);
        Long dungeonMasterId = gameRepositoryService.getUserIdByGameId(gameId);


        List<CompactGame.CompactRoom> compactRooms = new ArrayList<>();
        for (Room room: rooms){
            Long roomId = room.getId();

            List<CompactGame.CompactRoom.CompactItem> compactItems = getCompactItems(itemRooms, roomId);

            List<CompactGame.CompactRoom.CompactCharacter> compactCharacters = new ArrayList<>();

            List<CompactGame.CompactAction> compactActions = getCompactLocalActionsByRoom(localActions, roomId);

            compactRooms.add(assembleCompactRoom(room, roomId, compactItems, compactCharacters, compactActions));

        }

        CharacterPosTuple characterPosTuple = this.receiveCharacterJoinService.getCurrentCharacterWithPos(gameId, playerId);

        if (characterPosTuple != null) {
            for (CompactGame.CompactRoom compactRoom : compactRooms) {
                if (compactRoom.getCoordx() == characterPosTuple.getX() && compactRoom.getCoordy() == characterPosTuple.getY()) {
                    compactRoom.getCharactersInRoom().add(characterPosTuple.getCompactCharacter());
                }
            }
        }

        return CompactGame.builder().gameId(gameId).rooms(compactRooms).bannedPlayers(bannedPlayerIds).dungeonMasterId(dungeonMasterId).globalActions(compactGlobalActions).build();
    }

    private List<CompactGame.CompactAction> getCompactGlobalActions(List<GameAction> gameActions) {
        return gameActions.stream()
                    .map(gameAction -> CompactGame.CompactAction.builder()
                            .actionName(gameAction.getActionName())
                            .actionResult(gameAction.getActionResult())
                            .build())
                    .collect(Collectors.toList());
    }

    private List<CompactGame.CompactAction> getCompactLocalActionsByRoom(List<GameAction> localActions, Long roomId){
        return localActions.stream()
                .filter(localAction -> localAction.getRoomId().equals(roomId))
                .map(localAction -> CompactGame.CompactAction.builder()
                        .actionName(localAction.getActionName())
                        .actionResult(localAction.getActionResult())
                        .build())
                .collect(Collectors.toList());
    }

    private List<CompactGame.CompactRoom.CompactItem> getCompactItems(List<ItemRoom> itemRooms, Long roomId) {
        List<Long> itemIdsInRoom = itemRooms
                .stream()
                .filter(r -> r.getRoomId().equals(roomId))
                .map(ItemRoom::getItemId)
                .collect(Collectors.toList());
        List<Item> itemsInRoom = itemRepositoryService.findItemsByItemIds(itemIdsInRoom);
        return itemsInRoom.stream()
                .map(item -> CompactGame.CompactRoom.CompactItem.builder()
                        .itemName(item.getItemName())
                        .itemType(item.getItemType())
                        .itemValue(item.getItemValue())
                        .build())
                .collect(Collectors.toList());
    }

    private CompactGame.CompactRoom assembleCompactRoom(Room room, Long roomId, List<CompactGame.CompactRoom.CompactItem> compactItems, List<CompactGame.CompactRoom.CompactCharacter> compactCharacters, List<CompactGame.CompactAction> localActions) {
        return CompactGame.CompactRoom.builder()
                .roomName(room.getRoomName())
                .coordx(room.getCoordx())
                .coordy(room.getCoordy())
                .description(room.getDescrip())
                .roomId(roomId)
                .charactersInRoom(compactCharacters)
                .actions(Collections.emptyList())
                .compactItems(compactItems)
                .actions(localActions)
                .build();
    }
}
