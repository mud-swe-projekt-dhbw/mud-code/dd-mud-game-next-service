package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.ItemRoom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRoomRepository extends CrudRepository<ItemRoom, Long> {

    boolean existsByRoomIdAndItemIdAndGameId(Long roomId, Long itemId, Long gameId);

    List<ItemRoom> getItemRoomsByGameId(Long gameId);
}