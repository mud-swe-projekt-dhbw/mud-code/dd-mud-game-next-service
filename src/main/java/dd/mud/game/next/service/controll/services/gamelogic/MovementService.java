package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRoomRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.RoomRepositoryService;
import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.model.Room;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * this class processes movement actions
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MovementService {

    private final RoomRepositoryService roomRepositoryService;
//    private final CharacterRepositoryService characterRepositoryService;


    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns an answer for the Player after Moving
     */
    public String moveCharakterGetAnswer(MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
//        CompactGame.CompactRoom actualCompactRoom = getActualCompactRoomByPlayerId(mudKafkaMessage, compactGame);
        Integer updatedCoordx = getNewCoordx(mudKafkaMessage, compactGame);
        Integer updatedCoordy = getNewCoordy(mudKafkaMessage, compactGame);

        Room newRoom = roomRepositoryService.getRoomByGameIdAndCoordxAndCoordy(mudKafkaMessage.getGameId(), updatedCoordx, updatedCoordy);
//        Character character = characterRepositoryService.findCharacterByGameIdAndUserId(mudKafkaMessage.getGameId(), mudKafkaMessage.getId());

        return GameLogicStringConstants.ROOM_CHANGE_INFO +newRoom.getRoomName() +"\n"
                +newRoom.getDescrip() +"\n\n"
                +GameLogicStringConstants.AVAILABLE_COMMANDS + "\n"
                +GameLogicStringConstants.MOVE_WITH +GameLogicStringConstants.NORTH_SHORT +", " +GameLogicStringConstants.EAST_SHORT +", " +GameLogicStringConstants.SOUTH_SHORT +", " +GameLogicStringConstants.WEST_SHORT + "\n"
                +GameLogicStringConstants.PLAYER_INFORMATION + GameLogicStringConstants.INFO_SMALL +", " +GameLogicStringConstants.INFO_SHORT;
    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns the updated CompactGame after moving
     */
    public CompactGame getUpdatedCompactGameAfterMoving (MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        Integer updatedCoordx = getNewCoordx(mudKafkaMessage, compactGame);
        Integer updatedCoordy = getNewCoordy(mudKafkaMessage, compactGame);
        CompactGame.CompactRoom.CompactCharacter characterToChange = null;

        List<CompactGame.CompactRoom> roomList = compactGame.getRooms();


        for (CompactGame.CompactRoom room : roomList) {
            int index = 0;
            boolean delete = false;
            for (CompactGame.CompactRoom.CompactCharacter character : room.getCharactersInRoom()) {
                if (character.getPlayerId().equals(mudKafkaMessage.getId())) {
                    delete = true;
                    break;
                }
                index++;
            }
            if (delete) {
                characterToChange = room.getCharactersInRoom().remove(index);
                break;
            }
        }

        for (CompactGame.CompactRoom room : roomList) {
            if (room.getCoordx() == updatedCoordx && room.getCoordy() == updatedCoordy) {
                room.getCharactersInRoom().add(characterToChange);
            }
        }
        return compactGame;

    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns the actual Room of the Player depend on the PlayerId
     */
    public CompactGame.CompactRoom getActualCompactRoomByPlayerId(MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        CompactGame.CompactRoom compactRoom = CompactGame.CompactRoom.builder().build();
        List<CompactGame.CompactRoom> roomList = compactGame.getRooms();
        for (CompactGame.CompactRoom compactRoomFromCompactGame: roomList) {
            List<CompactGame.CompactRoom.CompactCharacter> compactCharacterList = compactRoomFromCompactGame.getCharactersInRoom();
            for (CompactGame.CompactRoom.CompactCharacter compactCharacterFromCompactGame: compactCharacterList) {
                if (mudKafkaMessage.getId().equals(compactCharacterFromCompactGame.getPlayerId())) {
                    compactRoom = compactRoomFromCompactGame;
                    break;
                }
            }
        }
        return compactRoom;
    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns the updated X-Coord after moving
     */
    public Integer getNewCoordx (MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        CompactGame.CompactRoom actualCompactRoom = getActualCompactRoomByPlayerId(mudKafkaMessage, compactGame);
        int actualCoordx = actualCompactRoom.getCoordx();

        if (mudKafkaMessage.getMessage().equals(GameLogicStringConstants.WEST_BIG) ||
                mudKafkaMessage.getMessage().equals(GameLogicStringConstants.WEST_SMALL) ||
                mudKafkaMessage.getMessage().equals(GameLogicStringConstants.WEST_SHORT)){
            return (actualCoordx - 1);
        }
        else  if (mudKafkaMessage.getMessage().equals(GameLogicStringConstants.EAST_BIG) ||
                mudKafkaMessage.getMessage().equals(GameLogicStringConstants.EAST_SMALL) ||
                mudKafkaMessage.getMessage().equals(GameLogicStringConstants.EAST_SHORT)){
            return (actualCoordx + 1);
        }
        else {
            return actualCoordx;
        }
    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return returns the updated Y-Coord after moving
     */
    public Integer getNewCoordy (MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        CompactGame.CompactRoom actualCompactRoom = getActualCompactRoomByPlayerId(mudKafkaMessage, compactGame);
        int actualCoordy = actualCompactRoom.getCoordy();

        if (mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_SHORT)){
            return  (actualCoordy - 1);
        }
        else  if (mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_SHORT)){
            return (actualCoordy + 1);
        }
        else {
            return actualCoordy;
        }
    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @param compactGame the actual compactGame
     * @return true if the room exists in the database, otherwise false
     */
    public boolean checkIfRoomExistsInDatabase (MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        return roomRepositoryService.existsByGameIdAndCoordxAndCoordy(mudKafkaMessage.getGameId(), getNewCoordx(mudKafkaMessage, compactGame), getNewCoordy(mudKafkaMessage, compactGame));
    }

    /**
     *
     * @param mudKafkaMessage the received mudKafkaMessage
     * @return true if the message of the received mudKafkaMessage is a direction, otherwise false
     */
    public boolean checkIfDirection (MudKafkaMessage mudKafkaMessage){
        return mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.NORTH_SHORT) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.EAST_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.EAST_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.EAST_SHORT) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.SOUTH_SHORT) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.WEST_BIG) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.WEST_SMALL) ||
                mudKafkaMessage.getMessage().equalsIgnoreCase(GameLogicStringConstants.WEST_SHORT);
    }
}
