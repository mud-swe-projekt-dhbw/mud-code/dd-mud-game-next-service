package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.CharakterRoom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharacterRoomRepository extends CrudRepository<CharakterRoom, Long> {

    boolean existsByCharakterIdAndRoomIdAndGameId(Long charakterId, Long roomId, Long gameId);

    List<CharakterRoom> getAllByGameId(Long gameId);


}
