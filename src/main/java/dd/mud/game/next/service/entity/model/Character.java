package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "charakter", schema = "public")
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String chName;

    private Long chLevel;

    private Long hp;

    private Long damage;

    private Long classId;

    private Long raceId;

    private Long userId;

    private Long gameId;

    private String description;

    private String personality;
}
