package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Ability;
import dd.mud.game.next.service.entity.model.GameAction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AbilityRepository  extends CrudRepository<Ability, Long> {

    boolean existsByAbilityNameAndGameId(String abilityName, Long gameId);

    Optional<Ability> findByAbilityNameAndGameId(String abilityName, Long gameId);

    List<Ability> findAllByGameId(Long gameId);

    List<Ability> getAllByGameId(Long gameId);
}
