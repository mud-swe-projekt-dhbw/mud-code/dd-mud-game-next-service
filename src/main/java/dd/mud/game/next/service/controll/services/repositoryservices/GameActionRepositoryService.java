package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.model.Game;
import dd.mud.game.next.service.entity.model.GameAction;
import dd.mud.game.next.service.entity.model.ItemRoom;
import dd.mud.game.next.service.entity.repository.GameActionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameActionRepositoryService {

    private final GameActionRepository gameActionRepository;


    public GameAction gameActions(Long gameId){
        return gameActionRepository.findAllByGameId(gameId).get();

    }

    public List<GameAction> getAllByGameIdAndIsGlobal(Long gameId, Boolean isGlobal){
        return gameActionRepository.getAllByGameIdAndIsGlobal(gameId, isGlobal);
    }

    public List<GameAction> getAllByGameIdAndRoomIdAndIsGlobal(Long gameId, Long roomId, Boolean isGlobal){
        return gameActionRepository.getAllByGameIdAndRoomIdAndIsGlobal(gameId, roomId, isGlobal);
    }
}
