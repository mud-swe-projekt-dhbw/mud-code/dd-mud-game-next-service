package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

// noArgsConstructor is necessary for the Database
@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(CharakterRoomId.class)
@Table(name = "charakterRoom", schema = "public")
public class CharakterRoom {

    @Id
    Long charakterId;

    @Id
    private Long roomId;

    private Long gameId;
}
