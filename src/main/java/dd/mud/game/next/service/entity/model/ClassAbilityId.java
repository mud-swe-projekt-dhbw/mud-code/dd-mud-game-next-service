package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ClassAbilityId implements Serializable {

    Long classId;

    Long abilityId;
}
