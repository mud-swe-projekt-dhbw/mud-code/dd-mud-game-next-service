package dd.mud.game.next.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class PlayerGameMemberId implements Serializable {

    Long userId;

    Long gameId;
}
