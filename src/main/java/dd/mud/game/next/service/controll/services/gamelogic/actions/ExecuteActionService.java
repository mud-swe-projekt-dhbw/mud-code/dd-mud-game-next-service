package dd.mud.game.next.service.controll.services.gamelogic.actions;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ExecuteActionService {

    private static final int ACTION_HP_INDEX = 0;
    private static final int ACTION_DMG_INDEX = 1;
    private static final int ACTION_RESULT_INDEX = 2;

    private final CharacterRepositoryService characterRepositoryService;

    public String executeActionAndReturnResultString (CompactGame.CompactAction compactAction, Long playerId, Long gameId, CompactGame.CompactRoom.CompactCharacter compactCharacter) {
        String[] action = compactAction.getActionResult().split("[;]");

        Long hpChange = Long.parseLong(action[ACTION_HP_INDEX]);
        Long dmgChange = Long.parseLong(action[ACTION_DMG_INDEX]);

        // Update Character in Compact game
        compactCharacter.setHp(compactCharacter.getHp() + hpChange);
        compactCharacter.setDamage(compactCharacter.getDamage() + dmgChange);

        Long finalHp = compactCharacter.getHp();
        Long finalDmg = compactCharacter.getDamage();

        this.characterRepositoryService.updateHpAndDamagePointsOfCharacter(playerId, gameId, finalHp, finalDmg);

        return action[ACTION_RESULT_INDEX];
    }
}
