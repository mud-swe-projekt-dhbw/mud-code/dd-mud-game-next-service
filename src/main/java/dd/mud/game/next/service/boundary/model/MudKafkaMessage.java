package dd.mud.game.next.service.boundary.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MudKafkaMessage {
    Long id;
    String message;
    Long gameId;
}