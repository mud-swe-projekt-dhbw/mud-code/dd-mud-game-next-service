package dd.mud.game.next.service.controll.services.gamelogic.actions;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.controll.services.repositoryservices.AbilityRepositoryService;
import dd.mud.game.next.service.entity.model.Ability;
import dd.mud.game.next.service.entity.model.ClassAbility;
import dd.mud.game.next.service.entity.model.RaceAbility;
import dd.mud.game.next.service.entity.repository.ClassAbilityRepository;
import dd.mud.game.next.service.entity.repository.RaceAbilityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ActionClassRaceCheckService {

    private final AbilityRepositoryService abilityRepositoryService;
    private final ClassAbilityRepository classAbilityRepository;
    private final RaceAbilityRepository raceAbilityRepository;

    public AbilityStatus playerIsAllowedToMakeAction (String actionName, Long gameId,
                                                      CompactGame.CompactRoom.CompactCharacter compactCharacter) {
        List<Ability> abilities = this.abilityRepositoryService.findAllByGameId(gameId);
        List<ClassAbility> classAbilities = this.classAbilityRepository.findAllByGameId(gameId);
        List<RaceAbility> raceAbilities = this.raceAbilityRepository.findAllByGameId(gameId);

        // Filter
        Optional<Ability> possibleAbility = abilities.stream().filter(ability -> ability.getAbilityName().equals(actionName)).findFirst();

        if (possibleAbility.isEmpty()) {
            return AbilityStatus.NOT_AVAILABLE;
        } else {

            List<ClassAbility> possibleClass = classAbilities.stream().filter(classAbility -> classAbility.getClassId().equals(compactCharacter.getClassId())).collect(Collectors.toList());
            List<RaceAbility> possibleRace = raceAbilities.stream().filter(raceAbility -> raceAbility.getRaceId().equals(compactCharacter.getRaceId())).collect(Collectors.toList());

            boolean allowed = false;

            for (ClassAbility aClass : possibleClass) {
                if (aClass.getAbilityId().equals(possibleAbility.get().getId())) allowed = true;
            }

            for (RaceAbility raceAbility : possibleRace) {
                if (raceAbility.getAbilityId().equals(possibleAbility.get().getId())) allowed = true;
            }

            return allowed ? AbilityStatus.ALLOWED : AbilityStatus.FORBIDDEN;
        }
    }
}
