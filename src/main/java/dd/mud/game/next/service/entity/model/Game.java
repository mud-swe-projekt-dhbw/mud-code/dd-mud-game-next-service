package dd.mud.game.next.service.entity.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

// noArgsConstructor is necessary for the Database
@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Table(name = "game", schema = "public")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String gameName;

    Long userId;
}
