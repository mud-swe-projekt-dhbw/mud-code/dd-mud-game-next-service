package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * this class is responsible for the leave logic of a game
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class LeaveGameService {

    /**
     *
     * @param mudKafkaMessage the received mudkafkamessage with the leave command
     * @param compactGame the actual compactGame
     * @return returns the compactgame of the input but without the compactcharacter of the player
     */

    public CompactGame getUpdatedCompactGameAfterGameExit(MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        for (CompactGame.CompactRoom room: compactGame.getRooms()){
            int index = 0;
            boolean playerFound = false;
            for (CompactGame.CompactRoom.CompactCharacter character: room.getCharactersInRoom()){
                if (character.getPlayerId().equals(mudKafkaMessage.getId())){
                    playerFound = true;
                    break;
                }
                index++;
            }
            if (playerFound) {
                room.getCharactersInRoom().remove(index);
                break;
            }
        }

        return  compactGame;
    }

    /**
     *
     * @return returns an constant with an exit message
     */

    public String leaveGame(){
        return GameLogicStringConstants.EXIT_GAME_LAST_MESSAGE;
    }
}
