package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RaceRepository extends CrudRepository<Race, Long> {

    boolean existsByRaceNameAndGameId (String raceName, Long gameId);

    List<Race> getRacesByGameId(Long gameId);

    List<Race> findAllByGameId(Long gameId);

    Optional<Race> findByRaceNameAndGameId(String raceName, Long gameId);

    Race getById(Long id);
}

