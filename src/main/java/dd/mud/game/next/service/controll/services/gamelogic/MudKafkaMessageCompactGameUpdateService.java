package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.gamelogic.actions.ActionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * class is the switch that calls multiple services to handle game logic and connects these services with the distributor service
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MudKafkaMessageCompactGameUpdateService {

    private final MovementService movementService;
    private final PlayerInformationService playerInformationService;
    private final LeaveGameService leaveGameService;
    private final ActionService actionService;

    /**
     * method is the switch
     * @param mudKafkaMessage the actual MudKafkaMessage received from the distributor service
     * @param compactGame the actual CompactGame received from the distributor service
     * @return returns an updated MudKafkaMessageCompactGamePair to the distributor service
     */
    public MudKafkaMessageCompactGamePair getUpdatedMessages(MudKafkaMessage mudKafkaMessage, CompactGame compactGame){
        String answer = GameLogicStringConstants.INVALID_INPUT;
        CompactGame updatedCompactGame = compactGame;

        if(mudKafkaMessage.getMessage().equals(GameLogicStringConstants.EXIT_GAME_COMMAND)){
            answer = leaveGameService.leaveGame();
            updatedCompactGame = leaveGameService.getUpdatedCompactGameAfterGameExit(mudKafkaMessage, compactGame);
        }
        else {
            if (movementService.checkIfDirection(mudKafkaMessage)) {
                if (movementService.checkIfRoomExistsInDatabase(mudKafkaMessage, compactGame)){
                    answer = movementService.moveCharakterGetAnswer(mudKafkaMessage, compactGame);
                    updatedCompactGame = movementService.getUpdatedCompactGameAfterMoving(mudKafkaMessage, compactGame);
                }else{
                    answer = GameLogicStringConstants.ROOM_DOESNT_EXIST;
                }
            } else {
                if (mudKafkaMessage.getMessage().equals(GameLogicStringConstants.INFO_BIG) ||
                        mudKafkaMessage.getMessage().equals(GameLogicStringConstants.INFO_SMALL) ||
                        mudKafkaMessage.getMessage().equals(GameLogicStringConstants.INFO_SHORT)) {
                    answer = playerInformationService.getPlayerInformation(mudKafkaMessage, compactGame);
                    updatedCompactGame = compactGame;
                } else {
                    return actionService.processActionCommand(compactGame, mudKafkaMessage);
                }
            }
        }



        return MudKafkaMessageCompactGamePair.builder()
                .mudKafkaMessage(MudKafkaMessage.builder()
                        .id(mudKafkaMessage.getId())
                        .gameId(mudKafkaMessage.getGameId())
                        .message(answer)
                        .build())
                .compactGame(updatedCompactGame)
                .build();
    }
}
