package dd.mud.game.next.service.controll.services.gamelogic.actions;

public enum AbilityStatus {
    ALLOWED,
    FORBIDDEN,
    NOT_AVAILABLE
}
