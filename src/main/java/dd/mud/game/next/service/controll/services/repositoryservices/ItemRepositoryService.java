package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Item;
import dd.mud.game.next.service.entity.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemRepositoryService {

    private final ItemRepository itemRepository;

    public List<Item> getItemsByGameId(long gameId){
        return itemRepository.getItemsByGameId(gameId);
    }

    public List<Item> findItemsByItemIds(List<Long> itemIds){
        return itemRepository.findItemsByIdIn(itemIds);
    }
}
