package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.producer.ChatServiceUpdateProducer;
import dd.mud.game.next.service.boundary.producer.DungeonMasterUpdateProducer;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCreatorService;
import dd.mud.game.next.service.entity.model.Character;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CompactGameUpdateService {

    private final DungeonMasterUpdateProducer dungeonMasterUpdateProducer;
    private final ChatServiceUpdateProducer chatServiceUpdateProducer;

    public void updateCompactGameOfWebservice(CompactGame updatedCompactGame) {
        dungeonMasterUpdateProducer.sendUpdatedGameToDungeonMaster(updatedCompactGame);
        chatServiceUpdateProducer.sendUpdatedGameToChatService(updatedCompactGame);
    }
}
