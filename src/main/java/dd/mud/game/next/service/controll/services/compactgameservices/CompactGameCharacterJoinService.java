package dd.mud.game.next.service.controll.services.compactgameservices;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.controll.services.compactgameservices.controlobject.CharacterPosTuple;
import dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRepositoryService;
import dd.mud.game.next.service.controll.services.repositoryservices.CharacterRoomRepositoryService;
import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.model.CharakterRoom;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CompactGameCharacterJoinService {


    private final CharacterRepositoryService characterRepositoryService;
    private final CompactCharakterService compactCharakterService;
    private final CharacterRoomRepositoryService characterRoomRepositoryService;
    private final ReceiveCharacterJoinService receiveCharacterJoinService;

    public CompactGame getCompactGameWithJoinedCharacter(Long playerId, CompactGame compactGame){
        if (!compactGame.getDungeonMasterId().equals(playerId)) {
            CharacterPosTuple characterPosTuple = this.receiveCharacterJoinService.getCurrentCharacterWithPos(compactGame.getGameId(), playerId);

            if (characterPosTuple != null) {
                for (CompactGame.CompactRoom compactRoom : compactGame.getRooms()) {
                    if (compactRoom.getCoordx() == characterPosTuple.getX() && compactRoom.getCoordy() == characterPosTuple.getY()) {
                        compactRoom.getCharactersInRoom().add(characterPosTuple.getCompactCharacter());
                    }
                }
            }
        }

        return compactGame;
    }

    public MudKafkaMessage getMudKafkaMessageAfterPlayerJoined(MudKafkaMessage mudKafkaMessage, CompactGame compactGame, String message){
        return MudKafkaMessage.builder().id(mudKafkaMessage.getId()).gameId(mudKafkaMessage.getGameId()).message(message).build();
    }
}
