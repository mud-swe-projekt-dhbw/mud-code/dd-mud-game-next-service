package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.UserData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDataRepository extends CrudRepository<UserData, Long> {
    UserData getById(Long id);
}
