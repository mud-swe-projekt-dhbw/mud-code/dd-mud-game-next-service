package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Game;
import dd.mud.game.next.service.entity.model.GameAction;
import dd.mud.game.next.service.entity.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameActionRepository extends CrudRepository<GameAction, Long> {

       Optional<GameAction> findAllByGameId(Long gameId);

       List<GameAction> getAllByGameId(Long gameId);


        List<GameAction> getAllByGameIdAndIsGlobal(Long gameId, Boolean isGlobal);

        List<GameAction> getAllByGameIdAndRoomIdAndIsGlobal(Long gameId, Long roomId, Boolean isGlobal);

        GameAction findGameActionByActionNameAndGameId(String actionName, Long gameId);

}

