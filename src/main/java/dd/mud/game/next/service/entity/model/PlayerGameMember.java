package dd.mud.game.next.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@IdClass(PlayerGameMemberId.class)
@Table(name = "playerGameMember", schema = "public")
public class PlayerGameMember {

    @Id
    Long userId;

    @Id
    Long gameId;
}
