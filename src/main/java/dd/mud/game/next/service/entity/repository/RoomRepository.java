package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Room;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room,Long> {

    boolean existsByRoomNameAndGameId(String roomName, Long gameId);
    List<Room> getRoomsByGameId(Long gameId);
    Optional<Room> findByRoomNameAndGameId(String roomName, Long gameId);
    Room getByGameIdAndCoordxAndCoordy (Long gameId, Integer coordx, Integer coordy);
    boolean existsByGameIdAndCoordxAndCoordy(Long gameId, Integer coordx, Integer coordy);
}
