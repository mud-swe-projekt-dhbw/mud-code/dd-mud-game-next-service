package dd.mud.game.next.service.controll.services.gamelogic;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import lombok.Builder;
import lombok.Data;

/**
 * a pair of the MudKafkaMessage and CompactGame needed for communication between services
 */

@Builder
@Data
public class MudKafkaMessageCompactGamePair {

    private MudKafkaMessage mudKafkaMessage;

    private CompactGame compactGame;

}
