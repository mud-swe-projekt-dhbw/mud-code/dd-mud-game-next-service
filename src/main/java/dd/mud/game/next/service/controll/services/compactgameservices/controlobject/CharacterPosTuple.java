package dd.mud.game.next.service.controll.services.compactgameservices.controlobject;

import dd.mud.game.next.service.boundary.model.CompactGame;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CharacterPosTuple {

    CompactGame.CompactRoom.CompactCharacter compactCharacter;
    int x;
    int y;
}
