package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.CharakterRoom;
import dd.mud.game.next.service.entity.model.ItemRoom;
import dd.mud.game.next.service.entity.repository.CharacterRoomRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharacterRoomRepositoryService {

    private final CharacterRoomRepository characterRoomRepository;

    public void saveNewCharacterRoom (Long gameId, Long ch_Id, Long roomId){
        characterRoomRepository.save(CharakterRoom.builder().gameId(gameId).charakterId(ch_Id).roomId(roomId).build());
    }

    public List<CharakterRoom> getCharacterRoomsByGameId(long gameId) {
        return characterRoomRepository.getAllByGameId(gameId);
    }
}
