package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.RaceAbility;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RaceAbilityRepository extends CrudRepository<RaceAbility, Long> {

    boolean existsByRaceIdAndAbilityIdAndGameId(Long raceId, Long abilityId, Long gameId);

    List<RaceAbility> findAllByGameId(Long gameId);
}
