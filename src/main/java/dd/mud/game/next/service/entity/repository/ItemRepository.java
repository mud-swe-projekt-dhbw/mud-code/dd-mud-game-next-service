package dd.mud.game.next.service.entity.repository;

import dd.mud.game.next.service.entity.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    boolean existsByItemNameAndGameId (String itemName, Long gameId);

    List<Item> getItemsByGameId(Long gameId);

    Item findByItemNameAndGameId(String itemName, Long gameId);

    List<Item> findItemsByIdIn(List<Long> ids);
}
