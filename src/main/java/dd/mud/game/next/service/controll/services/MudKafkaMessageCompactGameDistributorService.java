package dd.mud.game.next.service.controll.services;

import dd.mud.game.next.service.boundary.model.CompactGame;
import dd.mud.game.next.service.boundary.model.MudKafkaMessage;
import dd.mud.game.next.service.boundary.producer.PlayerAnswerProducer;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCharacterJoinService;
import dd.mud.game.next.service.controll.services.gamelogic.GameLogicStringConstants;
import dd.mud.game.next.service.controll.services.gamelogic.MudKafkaMessageCompactGamePair;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameCreatorService;
import dd.mud.game.next.service.controll.services.compactgameservices.CompactGameUpdateService;
import dd.mud.game.next.service.controll.services.gamelogic.MudKafkaMessageCompactGameUpdateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * this service is the aggregator that sends messages to producer and listener services that handle the communication with other webservices.
 * The service also receives and send messages to the compactGameUpdateService that is the switch for the game logic
 * The service holds the different games in storage
 */

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MudKafkaMessageCompactGameDistributorService {

    /**
     * A hashmap with all created games that have started. GameIds are the keys
     */
    HashMap<Long, CompactGame> games = new HashMap<>();

    private final MudKafkaMessageCompactGameUpdateService mudKafkaMessageCompactGameUpdateService;
    private final CompactGameUpdateService compactGameUpdateService;
    private final PlayerAnswerProducer playerAnswerProducer;
    private final CompactGameCreatorService compactGameCreatorService;
    private final CompactGameCharacterJoinService compactGameCharacterJoinService;

    /**
     * this method receives the mudkafkamessage and compactGameMessage from other services, calls services that process this input and then calls services that send messages back to other services
     * @param mudKafkaMessage the new MudKafkaMessage received from the listener service
     */

    public void handleMudKafkaMessageInput(MudKafkaMessage mudKafkaMessage){
        CompactGame compactGame = getCompactGameByGameId(mudKafkaMessage.getGameId());

        MudKafkaMessage updatedMudKafkaMessage;
        try {
            if (games.containsKey(mudKafkaMessage.getGameId())) {
                if (games.get(mudKafkaMessage.getGameId()).getBannedPlayers().contains(mudKafkaMessage.getId())) {
                    updatedMudKafkaMessage = updateMudKafkaMessageByMessage(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), GameLogicStringConstants.PLAYER_BANNED);
                } else {
                    if (mudKafkaMessage.getMessage().equals(GameLogicStringConstants.GAME_JOIN_COMMAND)) {
                        CompactGame compactGameWithPlayer = compactGameCharacterJoinService.getCompactGameWithJoinedCharacter(mudKafkaMessage.getId(), compactGame);
                        String descriptionOfZeroZeroRoom = getDescriptionOfZeroZeroRoom(compactGame);
                        updatedMudKafkaMessage = compactGameCharacterJoinService.getMudKafkaMessageAfterPlayerJoined(mudKafkaMessage, compactGame, descriptionOfZeroZeroRoom);
                        updateCompactGame(mudKafkaMessage, compactGameWithPlayer);
                    } else if (mudKafkaMessage.getId().equals(compactGame.getDungeonMasterId()) && mudKafkaMessage.getMessage().equals(GameLogicStringConstants.RESET_GAME_COMMAND)) {
                        updatedMudKafkaMessage = updateMudKafkaMessageByMessage(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), GameLogicStringConstants.RESET_MESSAGE);
                        games.remove(mudKafkaMessage.getGameId());
                    } else {
                        MudKafkaMessageCompactGamePair mudKafkaMessageCompactGamePair = mudKafkaMessageCompactGameUpdateService.getUpdatedMessages(mudKafkaMessage, compactGame);
                        updatedMudKafkaMessage = mudKafkaMessageCompactGamePair.getMudKafkaMessage();
                            updateCompactGame(mudKafkaMessage, mudKafkaMessageCompactGamePair.getCompactGame());
                    }
                }
            } else {
                if (mudKafkaMessage.getMessage().startsWith(GameLogicStringConstants.GAME_CREATE_COMMAND)) {
                    updatedMudKafkaMessage = createGame(mudKafkaMessage);
                } else {
                    updatedMudKafkaMessage = updateMudKafkaMessageByMessage(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), GameLogicStringConstants.GAME_CREATION_FAILED_OR_GAME_DELETED);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
            updatedMudKafkaMessage = updateMudKafkaMessageByMessage(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), GameLogicStringConstants.INVALID_INPUT);
        }
        playerAnswerProducer.sendAnswerToPlayer(updatedMudKafkaMessage);
    }

    /**
     * this method calls the compactGameCreatorService, receives this new CompactGame and saves it in the hashmap. then it returns an mudkafkamessage with an initial message for the player
     * @param mudKafkaMessage the mudkafkamessage with an initial message
     * @return returns an mudkafkamessage with an initial message for the player
     */

    private MudKafkaMessage createGame(MudKafkaMessage mudKafkaMessage) {
        CompactGame compactGame;
        MudKafkaMessage updatedMudKafkaMessage;
        CompactGame newCompactGame = compactGameCreatorService.createCompactGame(mudKafkaMessage.getId(), mudKafkaMessage.getGameId());
        games.put(mudKafkaMessage.getGameId(), newCompactGame);
        compactGame = newCompactGame;
        updatedMudKafkaMessage = updateMudKafkaMessageByMessage(mudKafkaMessage.getId(), mudKafkaMessage.getGameId(), getDescriptionOfZeroZeroRoom(compactGame));
        updateCompactGame(mudKafkaMessage, compactGame);
        return updatedMudKafkaMessage;
    }

    /**
     * this method calls the compactGameUpdateService to call the webservices to update the compactGame and updates the compact game in the hashmap
     * @param mudKafkaMessage the updated mudkafkamessage
     * @param compactGame the updated compactgame
     */

    private void updateCompactGame(MudKafkaMessage mudKafkaMessage, CompactGame compactGame) {
        compactGameUpdateService.updateCompactGameOfWebservice(compactGame);
        games.replace(mudKafkaMessage.getGameId(), compactGame);
    }

    /**
     * replaces the game in the hashmap after an dungeonmaster update
     * @param newCompactGame the updated compactGame of the dungeonMaster
     */

    public void handleCompactGameInputFromDungeonMaster(CompactGame newCompactGame){
        games.replace(newCompactGame.getGameId(), newCompactGame);
    }

    /**
     *
     * @param playerId the player id
     * @param gameId the id of the game
     * @param updatedMessage the updated string message that is part of the mudkafkamessage
     * @return returns an assembled mudkafkamessage
     */

    private MudKafkaMessage updateMudKafkaMessageByMessage(Long playerId, Long gameId, String updatedMessage) {
        MudKafkaMessage updatedMudKafkaMessage;
        updatedMudKafkaMessage = MudKafkaMessage.builder()
                .id(playerId)
                .gameId(gameId)
                .message(updatedMessage)
                .build();
        return updatedMudKafkaMessage;
    }

    /**
     *
     * @param gameId the id of the game
     * @return returns the compactGame with the game Id that matches
     */

    public CompactGame getCompactGameByGameId(Long gameId){
        if (games.containsKey(gameId)){
            return games.get(gameId);
        }
        return null;
    }

    /**
     *
     * @param compactGame the actual compactGame
     * @return returns an answer message for the player. The message describes the room in which the player is and is only sended if the player is in the room with the coordinates x=0 and y=0, the start room
     */

    private String getDescriptionOfZeroZeroRoom (CompactGame compactGame) {
        for (CompactGame.CompactRoom room : compactGame.getRooms()) {
            if (room.getCoordx() == 0 && room.getCoordy() == 0) {
                return GameLogicStringConstants.YOU_ARE_IN_ROOM + room.getRoomName() + "\n" + room.getDescription();
            }
        }
        return GameLogicStringConstants.ZERO_ROOM_NOT_FOUND;
    }
}
