package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Character;
import dd.mud.game.next.service.entity.repository.CharacterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharacterRepositoryService {

    private final CharacterRepository characterRepository;

    public Character findCharacterByGameIdAndUserId(Long gameId, Long userId){
        return characterRepository.findCharacterByGameIdAndUserId(gameId, userId).orElse(null);
    }

    public List<Character> findCharactersByCharacterIds(List<Long> characterIds){
        return characterRepository.findCharactersByIdIn(characterIds);
    }

    public void updateHpAndDamagePointsOfCharacter(Long playerId, Long gameId, Long hp, Long damage){
        Optional<Character> characterEntity = characterRepository.findCharacterByGameIdAndUserId(gameId, playerId);
        if (characterEntity.isPresent()) {
            Character character = characterEntity.get();
            character.setHp(hp);
            character.setDamage(damage);
            characterRepository.save(character);
        }
    }
}
