package dd.mud.game.next.service.controll.services.repositoryservices;

import dd.mud.game.next.service.entity.model.Class;
import dd.mud.game.next.service.entity.repository.ClassRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClassRepositoryService {

    private final ClassRepository classRepository;

    public List<Class> getClassesByGameId(Long gameId) {
        List<Class> classes = new ArrayList<>();
        classRepository.findAllByGameId(gameId).forEach(classes::add);
        return classes;
    }

    public Class getClassByClassId(Long id){
        return classRepository.getById(id);
    }
}
